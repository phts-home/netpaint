

del bin-all\com\tsarik\programs\netpaint_client\actions\*.class
del bin-all\com\tsarik\programs\netpaint_client\components\*.class
del bin-all\com\tsarik\programs\netpaint_client\frames\*.class
del bin-all\com\tsarik\programs\netpaint_client\lib\*.class
del bin-all\com\tsarik\programs\netpaint_client\*.class

copy bin\com\tsarik\programs\netpaint_client\actions\*.class     bin-all\com\tsarik\programs\netpaint_client\actions\*.class
copy bin\com\tsarik\programs\netpaint_client\components\*.class  bin-all\com\tsarik\programs\netpaint_client\components\*.class
copy bin\com\tsarik\programs\netpaint_client\frames\*.class      bin-all\com\tsarik\programs\netpaint_client\frames\*.class
copy bin\com\tsarik\programs\netpaint_client\lib\*.class         bin-all\com\tsarik\programs\netpaint_client\lib\*.class
copy bin\com\tsarik\programs\netpaint_client\*.class             bin-all\com\tsarik\programs\netpaint_client\*.class

cd bin-all
e:\Java\jdk1.6.0_18\bin\jar cvfm ../NetPaint.jar ../MANIFEST.MF *
pause