package com.tsarik.programs.netpaint_client.frames;

import java.awt.Component;

import javax.swing.*;

import java.awt.*;

import com.tsarik.dialogs.*;

public class ColorChooserDialog extends AbstractAplicationDialog {
	
	public ColorChooserDialog(MainFrame owner) {
		super(owner, "Color", 460, 350);
		this.owner = owner;
	}
	
	@Override
	protected int apply() {
		color = c.getColor();
		owner.getDrawingArea().setColor(color);
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected void refresh() {
		color = owner.getDrawingArea().getColor();
	}
	
	
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		
		c = new JColorChooser();
		c.setPreviewPanel(new JPanel());
		this.addComponentToGrid(c, 0, 0, 1, 1, panel);
		
		return panel;
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
		c.setColor(color);
	}
	
	private Color color;
	private JColorChooser c;
	private MainFrame owner;
	
}
