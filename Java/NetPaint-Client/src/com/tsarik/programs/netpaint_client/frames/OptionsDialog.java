package com.tsarik.programs.netpaint_client.frames;

import java.awt.Component;
import java.awt.GridBagLayout;

import javax.swing.*;

import com.tsarik.programs.netpaint_client.*;
import com.tsarik.programs.netpaint_client.lib.*;
import com.tsarik.dialogs.*;


public class OptionsDialog extends AbstractAplicationDialog {
	
	public OptionsDialog(MainFrame owner) {
		super(owner, "Options", 400, 300);
		this.owner = owner;
	}
	
	@Override
	protected int apply() {
		int cp = Parameters.CON_PORT;
		try {
			cp = Integer.valueOf(tfConPort.getText());
		} catch (NumberFormatException e) {
		}
		
		if ( (!tfConServerhost.getText().equals(Parameters.CON_SERVERHOST)) || (cp != Parameters.CON_PORT) || (!tfConName.getText().equals(Parameters.CON_NAME)) ) {
			if (ConnectionManager.getInstance().isConnectionCreated()) {
				Parameters.showInfoMessage(this, Parameters.MESSAGE_AFTER_RECONNECT);
			}
		}
		
		Parameters.CON_SERVERHOST = tfConServerhost.getText();
		Parameters.CON_PORT = cp;
		Parameters.CON_NAME = tfConName.getText();
		
		Parameters.IMAGE_WIDTH = ((SpinnerNumberModel)tfImageWidth.getModel()).getNumber().intValue();
		Parameters.IMAGE_HEIGHT = ((SpinnerNumberModel)tfImageHeight.getModel()).getNumber().intValue();
		
		if ( (owner.getDrawingArea().getCanvasWidth() != Parameters.IMAGE_WIDTH) || (owner.getDrawingArea().getCanvasHeight() != Parameters.IMAGE_HEIGHT) ) {
			if (ConnectionManager.getInstance().isClientConnected()) {
				ConnectionManager.getInstance().sendImageDimensionData(Parameters.IMAGE_WIDTH, Parameters.IMAGE_HEIGHT);
			} else {
				owner.getDrawingArea().setCanvasSize(Parameters.IMAGE_WIDTH, Parameters.IMAGE_HEIGHT);
			}
		}
		return APPLY_CHECK_OK;
	}
	
	@Override
	protected void refresh() {
		tfConServerhost.setText(Parameters.CON_SERVERHOST);
		tfConPort.setText(Integer.toString(Parameters.CON_PORT));
		tfConName.setText(Parameters.CON_NAME);
		((SpinnerNumberModel)tfImageWidth.getModel()).setValue(Parameters.IMAGE_WIDTH);
		((SpinnerNumberModel)tfImageHeight.getModel()).setValue(Parameters.IMAGE_HEIGHT);
	}
	
	@Override
	protected Component createComponentPanel() {
		JPanel panel = (JPanel)super.createComponentPanel();
		tabbedPane = new JTabbedPane();
		tabbedPane.add("Connection", createConnectionTab());
		tabbedPane.add("Image", createImageTab());
		addComponentToGrid(tabbedPane, 0, 0, 1, 1, 0, panel);
		return panel;
	}
	
	private MainFrame owner;
	
	private JTabbedPane tabbedPane;
	
	private JTextField tfConServerhost;
	private JTextField tfConPort;
	private JTextField tfConName;
	
	private JSpinner tfImageWidth;
	private JSpinner tfImageHeight;
	
	private Component createConnectionTab() {
		JPanel panel = new JPanel();
		GridBagLayout layout = new GridBagLayout();
		panel.setLayout(layout);
		
		tfConServerhost = new JTextField();
		tfConPort = new JTextField();
		tfConName = new JTextField();
		
		addComponentToGrid(new JLabel("Server host:"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(tfConServerhost, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Port:"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(tfConPort, 1, 1, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Name:"), 0, 2, 1, 1, 1, panel);
		addComponentToGrid(tfConName, 1, 2, 1, 1, 4, panel);
		return panel;
	}
	
	private Component createImageTab() {
		JPanel panel = new JPanel();
		GridBagLayout layout = new GridBagLayout();
		panel.setLayout(layout);
		
		tfImageWidth = new JSpinner(new SpinnerNumberModel(500, 1, 1000, 1));
		tfImageHeight = new JSpinner(new SpinnerNumberModel(500, 1, 1000, 1));
		
		addComponentToGrid(new JLabel("Width:"), 0, 0, 1, 1, 1, panel);
		addComponentToGrid(tfImageWidth, 1, 0, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Height:"), 0, 1, 1, 1, 1, panel);
		addComponentToGrid(tfImageHeight, 1, 1, 1, 1, 4, panel);
		addComponentToGrid(new JLabel("Warning: Image will be cleared after resize"), 0, 2, 2, 1, 1, panel);
		return panel;
	}
	

}
