package com.tsarik.programs.netpaint_client.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.image.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;

import javax.swing.*;
import javax.imageio.*;

import org.w3c.dom.*;

import com.tsarik.application.*;
import com.tsarik.application.preferences.*;
import com.tsarik.documents.SimpleXMLDocument;
import com.tsarik.programs.netpaint_client.*;
import com.tsarik.programs.netpaint_client.actions.*;
import com.tsarik.programs.netpaint_client.components.DrawingArea;

public class MainFrame extends AbstractMainFrame implements PreferencesCustomizable, PreferencesApplicable {
	
	public MainFrame(NetPaintClient application) {
		super(application);
		
		this.setTitle(Parameters.PROGRAM_NAME);
		try {
			this.setIconImage(ImageIO.read(new File("res/icon_small.gif")));
		} catch (IOException e) {
		}
		this.addComponentListener(new WindowResizeHandler());
	}
	
	public void onClose() {
		application.exit();
	}
	
	
	
	@Override
	public void loadPreferences(SimpleXMLDocument doc) {
		Element el = doc.getElement(null, "main_frame");
		
		Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize();
		Parameters.MAINFRAME_STATE = doc.readIntegerValue(el, "state", Frame.NORMAL);
		Parameters.MAINFRAME_WIDTH = doc.readIntegerValue(el, "width", 620);
		Parameters.MAINFRAME_HEIGHT = doc.readIntegerValue(el, "height", 500);
		Parameters.MAINFRAME_LEFT = doc.readIntegerValue(el, "left", scrSize.width/2 - Parameters.MAINFRAME_WIDTH/2);
		Parameters.MAINFRAME_TOP = doc.readIntegerValue(el, "top", scrSize.height/2 - Parameters.MAINFRAME_HEIGHT/2);
		
		el = doc.getElement(null, "connection");
		Parameters.CON_SERVERHOST = doc.readStringValue(el, "host", "localhost");
		Parameters.CON_PORT = doc.readIntegerValue(el, "port", 4444);
		Parameters.CON_NAME = doc.readStringValue(el, "name", "User");
		
		el = doc.getElement(null, "image");
		Parameters.IMAGE_WIDTH = doc.readIntegerValue(el, "width", 500);
		Parameters.IMAGE_HEIGHT = doc.readIntegerValue(el, "height", 500);
		
		this.setLocation(Parameters.MAINFRAME_LEFT, Parameters.MAINFRAME_TOP);
		this.setSize(Parameters.MAINFRAME_WIDTH, Parameters.MAINFRAME_HEIGHT);
		this.setExtendedState(Parameters.MAINFRAME_STATE);
	}
	
	@Override
	public void savePreferences(SimpleXMLDocument doc) {
		Element el = doc.getElement(null, "main_frame");
		doc.addProperty(el, "state", this.getExtendedState());
		this.setExtendedState(Frame.NORMAL);
		doc.addProperty(el, "left", this.getX());
		doc.addProperty(el, "top", this.getY());
		doc.addProperty(el, "width", this.getWidth());
		doc.addProperty(el, "height", this.getHeight());
		
		el = doc.getElement(null, "connection");
		doc.addProperty(el, "host", Parameters.CON_SERVERHOST);
		doc.addProperty(el, "port", Parameters.CON_PORT);
		doc.addProperty(el, "name", Parameters.CON_NAME);
		
		el = doc.getElement(null, "image");
		doc.addProperty(el, "width", Parameters.IMAGE_WIDTH);
		doc.addProperty(el, "height", Parameters.IMAGE_HEIGHT);
	}
	
	@Override
	public void applyPreferences() {
		drawingArea.setCanvasSize(Parameters.IMAGE_WIDTH, Parameters.IMAGE_HEIGHT);
		
		splitPane.setDividerLocation(0.8);
	}
	
	public void loadImage() {
		try {
			if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				BufferedImage image = ImageIO.read(fileChooser.getSelectedFile());
				if (image == null) {
					return;
				}
				drawingArea.sendImage(image);
			}
		} catch (IOException e) {
			Parameters.showErrorMessage(this, Parameters.MESSAGE_IO_ERROR, e.getMessage());
		}
	}
	
	public void saveImage() {
		try {
			if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
				File out = BMPFileFilter.addExtension(fileChooser.getSelectedFile());
				ImageIO.write(drawingArea.getImage(), "bmp", out);
			}
		} catch (IOException e) {
			Parameters.showErrorMessage(this, Parameters.MESSAGE_IO_ERROR, e.getMessage());
		}
	}
	
	public void showOptionsDialog() {
		optionsDialog.setVisible(true);
	}
	
	public ColorChooserDialog getColorChooser() {
		return colorChooser;
	}
	
	public DrawingArea getDrawingArea() {
		return drawingArea;
	}
	
	public JList getClientList() {
		return clientList;
	}
	
	public void repaintToolBar() {
		toolBar.repaint();
	}
	
	private OptionsDialog optionsDialog;
	private DrawingArea drawingArea;
	private JList clientList;
	private JToolBar toolBar;
	private JSplitPane splitPane;
	private JFileChooser fileChooser;
	private ColorChooserDialog colorChooser;
	
	
	protected void createComponents() {
		AbstractAction connectToServerAction = new ConnectToServerAction(this);
		AbstractAction closeConnectionAction = new CloseConnectionAction(this);
		
		ChooseColorAction ˝hooseColorAction = new ChooseColorAction(this);
		
		AbstractAction penAction = new SetToolAction(this, "Pen", DrawingArea.TOOL_PEN);
		AbstractAction rectAction = new SetToolAction(this, "Rectangle", DrawingArea.TOOL_RECT);
		AbstractAction circAction = new SetToolAction(this, "Circle", DrawingArea.TOOL_CIRCLE);
		AbstractAction eraserAction = new SetToolAction(this, "Eraser", DrawingArea.TOOL_ERASER);
		AbstractAction filledrectAction = new SetToolAction(this, "Filled Rectangle", DrawingArea.TOOL_FILLEDRECT);
		AbstractAction filledcircAction = new SetToolAction(this, "Filled Circle", DrawingArea.TOOL_FILLEDCIRCLE);
		
		AbstractAction setPenWidth1Action = new SetPenWidthAction(this, "Thin", 1);
		AbstractAction setPenWidth2Action = new SetPenWidthAction(this, "Normal", 2);
		AbstractAction setPenWidth3Action = new SetPenWidthAction(this, "Thick", 3);
		
		JMenu fileMenu = new JMenu("File");
		fileMenu.add(new LoadImageAction(this));
		fileMenu.add(new SaveImageAction(this));
		fileMenu.addSeparator();
		fileMenu.add(new ExitAction(this));
		
		JMenu connectionMenu = new JMenu("Connection");
		connectionMenu.add(connectToServerAction);
		connectionMenu.addSeparator();
		connectionMenu.add(closeConnectionAction);
		
		JMenu paintingMenu = new JMenu("Painting");
		paintingMenu.add(new ClearAction(this));
		paintingMenu.addSeparator();
		paintingMenu.add(penAction);
		paintingMenu.add(rectAction);
		paintingMenu.add(circAction);
		paintingMenu.add(filledrectAction);
		paintingMenu.add(filledcircAction);
		paintingMenu.add(eraserAction);
		paintingMenu.addSeparator();
		paintingMenu.add(˝hooseColorAction);
		paintingMenu.addSeparator();
		paintingMenu.add(setPenWidth1Action);
		paintingMenu.add(setPenWidth2Action);
		paintingMenu.add(setPenWidth3Action);
		
		JMenu toolsMenu = new JMenu("Tools");
		toolsMenu.add(new ShowOptionsDialogAction(this));
		
		JMenu helpMenu = new JMenu("Help");
		helpMenu.add(new AboutAction(this));
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);
		menuBar.add(connectionMenu);
		menuBar.add(paintingMenu);
		menuBar.add(toolsMenu);
		menuBar.add(helpMenu);
		
		toolBar = new JToolBar();
		toolBar.add(connectToServerAction);
		toolBar.add(closeConnectionAction);
		toolBar.addSeparator();
		toolBar.add(˝hooseColorAction);
		toolBar.addSeparator();
		toolBar.add(penAction);
		toolBar.add(rectAction);
		toolBar.add(circAction);
		toolBar.add(filledrectAction);
		toolBar.add(filledcircAction);
		toolBar.add(eraserAction);
		toolBar.addSeparator();
		toolBar.add(setPenWidth1Action);
		toolBar.add(setPenWidth2Action);
		toolBar.add(setPenWidth3Action);
		
		optionsDialog = new OptionsDialog(this);
		
		fileChooser = new JFileChooser();
		fileChooser.addChoosableFileFilter(new BMPFileFilter());
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setCurrentDirectory(new File("."));
		fileChooser.setMultiSelectionEnabled(false);
		
		colorChooser = new ColorChooserDialog(this);
		
		drawingArea = new DrawingArea(this);
		drawingArea.addDrawingPanelListener(˝hooseColorAction);
		
		JScrollPane scrollPane = new JScrollPane(drawingArea);
		
		clientList = new JList();
		clientList.setModel(new DefaultListModel());
		
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scrollPane, clientList);
		splitPane.setOneTouchExpandable(true);
		
		setJMenuBar(menuBar);
		this.add(toolBar, BorderLayout.NORTH);
		this.add(splitPane, BorderLayout.CENTER);
	}
	
	
	
	private class WindowResizeHandler implements ComponentListener {
		
		@Override
		public void componentHidden(ComponentEvent e) {}
		
		@Override
		public void componentMoved(ComponentEvent e) {}
		
		@Override
		public void componentResized(ComponentEvent e) {
			splitPane.setDividerLocation(0.8);
		}
		
		@Override
		public void componentShown(ComponentEvent e) {}
		
	}
	
}
