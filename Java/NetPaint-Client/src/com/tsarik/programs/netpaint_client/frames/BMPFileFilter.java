package com.tsarik.programs.netpaint_client.frames;

import java.io.File;
import javax.swing.filechooser.FileFilter;

class BMPFileFilter extends FileFilter {
	
	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}
		return isBmpExtension(f);
	}
	
	@Override
	public String getDescription() {
		return "BMP Images";
	}
	
	public static boolean isBmpExtension(File f) {
		String ext = null;
		String s = f.getName();
		int i = s.lastIndexOf('.');
		
		if (i > 0 &&  i < s.length() - 1) {
			ext = s.substring(i+1).toLowerCase();
		}
		
		if (ext != null) {
			if (ext.equalsIgnoreCase("bmp")) {
				return true;
			}
		}
		return false;
	}
	
	public static File addExtension(File f) {
		if (!BMPFileFilter.isBmpExtension(f)) {
			String n = f.getName().concat(".bmp");
			f = new File(f.getParentFile(), n);
		}
		return f;
	}
	
}
