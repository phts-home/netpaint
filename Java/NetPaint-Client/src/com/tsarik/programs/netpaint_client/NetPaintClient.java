package com.tsarik.programs.netpaint_client;

import javax.swing.JFrame;

import com.tsarik.application.*;
import com.tsarik.application.preferences.*;
import com.tsarik.programs.netpaint_client.frames.*;
import com.tsarik.programs.netpaint_client.lib.*;


public class NetPaintClient extends AbstractApplication {
	
	public static void main(String[] args) {
		new NetPaintClient();
	}
	
	public NetPaintClient() {
		preferences = new SimpleProgramPreferences(Parameters.PROGRAM_PREFERENCES_PATH, "preferences"); 
		
		mainFrame = new MainFrame(this);
		
		preferences.addCustomizable(mainFrame);
		preferences.addApplicable(mainFrame);
		
		preferences.load();
		
		mainFrame.setVisible(true);
		
		preferences.apply();
		
		try {
			FromJavaToNativeInterface.init();
			FromNativeToJavaInterface.init(this);
			ConnectionManager.createInstance(this);
		} catch (UnsatisfiedLinkError e) {
			Parameters.showErrorMessage(mainFrame, Parameters.MESSAGE_DLL_ERROR);
			mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			mainFrame.dispose();
		}
	}
	
	public void exit() {
		preferences.save();
		
		try {
			ConnectionManager.getInstance().closeConnection();
		} catch (Exception e) {
		}
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.dispose();
	}
	
	public MainFrame getMainFrame() {
		return mainFrame;
	}
	
	private SimpleProgramPreferences preferences;
	private MainFrame mainFrame;
	
}
