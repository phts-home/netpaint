package com.tsarik.programs.netpaint_client;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Parameters {
	
	public static final String PROGRAM_NAME = "NetPaint";
	public static final String PROGRAM_VERSION = "1.0.0";
	public static final int PROGRAM_BUILD = 10;
	public static final int PROGRAM_YEAR = 2010;
	public static final String PROGRAM_AUTHOR = "Phil Tsarik";
	public static final String PROGRAM_PREFERENCES_PATH = "preferences.xml";
	
	public static String CON_SERVERHOST;
	public static int CON_PORT;
	public static String CON_NAME;
	
	public static int MAINFRAME_STATE;
	public static int MAINFRAME_LEFT;
	public static int MAINFRAME_TOP;
	public static int MAINFRAME_WIDTH;
	public static int MAINFRAME_HEIGHT;
	
	public static int IMAGE_WIDTH;
	public static int IMAGE_HEIGHT;
	
	public static String MESSAGE_DLL_ERROR = "Failed to access to the native DLL";
	public static String MESSAGE_CLIENT_ALREADYCREATED = "Connection is already created";
	public static String MESSAGE_CLIENT_ALREADYCONNECTED = "You have already connected to the server";
	public static String MESSAGE_SERVER_FULL = "Connection refused. Server is full";
	public static String MESSAGE_CONNECTION_LOST = "Connection is lost";
	public static String MESSAGE_CONNECTION_FAILED = "Failed to connect to server";
	public static String MESSAGE_SOCKET_FAILED = "Unable to create connection";
	public static String MESSAGE_ALREADY_CLOSED = "Connection is already closed";
	public static String MESSAGE_UNABLE_CLEAR = "Image can be cleared only by owner";
	public static String MESSAGE_AFTER_RECONNECT = "New connection settings will take effect after reconnect";
	public static String MESSAGE_UNABLE_RESIZE = "Image can be resized only by owner";
	public static String MESSAGE_UNABLE_LOADIMAGE = "Image can be loaded only by owner";
	public static String MESSAGE_IO_ERROR = "I/O error occurred";
	
	public static void showErrorMessage(Component parentComponent, String message, String errorMessage) {
		JOptionPane.showMessageDialog(parentComponent, message+"\n\nError message:\n\n"+"<html><textarea cols=20 rows=2>"+errorMessage.replaceAll("[\n\r]", " ")+"</textarea></html>", Parameters.PROGRAM_NAME, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void showErrorMessage(Component parentComponent, String message) {
		JOptionPane.showMessageDialog(parentComponent, message, Parameters.PROGRAM_NAME, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void showInfoMessage(Component parentComponent, String message) {
		JOptionPane.showMessageDialog(parentComponent, message, Parameters.PROGRAM_NAME, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void showAbout(Component parentComponent) {
		ImageIcon icon = new ImageIcon("res/icon_large.gif");
		if (icon != null) {
			JOptionPane.showMessageDialog(parentComponent,
					"<html><b>"
						+"<h3>"+Parameters.PROGRAM_NAME+"</h3>"
						+"<br>Version: "+Parameters.PROGRAM_VERSION+"."+Parameters.PROGRAM_BUILD+"</b></html>\n\n"
						+"<html>"
						+"&#169; "+Parameters.PROGRAM_AUTHOR+", "+Parameters.PROGRAM_YEAR
						+"<br>&nbsp;</html>",
					"About", JOptionPane.INFORMATION_MESSAGE, icon);
		} else {
			JOptionPane.showMessageDialog(parentComponent,
					"<html><b>"
						+"<h3>"+Parameters.PROGRAM_NAME+"</h3>"
						+"<br>Version: "+Parameters.PROGRAM_VERSION+"."+Parameters.PROGRAM_BUILD+"</b></html>\n\n"
						+"<html>"
						+"&#169; "+Parameters.PROGRAM_AUTHOR+", "+Parameters.PROGRAM_YEAR
						+"<br>&nbsp;</html>",
					"About", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
}
