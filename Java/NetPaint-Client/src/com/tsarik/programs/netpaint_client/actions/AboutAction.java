package com.tsarik.programs.netpaint_client.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import com.tsarik.programs.netpaint_client.Parameters;
import com.tsarik.programs.netpaint_client.frames.MainFrame;

public class AboutAction extends AbstractProgramMenuAction {
	
	public AboutAction(MainFrame owner) {
		super(owner, "About...");
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		Parameters.showAbout(owner);
	}
	
}
