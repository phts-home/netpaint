package com.tsarik.programs.netpaint_client.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.netpaint_client.frames.MainFrame;

public class ExitAction extends AbstractProgramMenuAction {
	
	public ExitAction(MainFrame owner) {
		super(owner, "Exit");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.onClose();
	}

}
