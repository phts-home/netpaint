package com.tsarik.programs.netpaint_client.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.netpaint_client.frames.MainFrame;

public class ShowOptionsDialogAction extends AbstractProgramMenuAction {
	
	public ShowOptionsDialogAction(MainFrame owner) {
		super(owner, "Options...");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_MASK));
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.showOptionsDialog();
	}

}
