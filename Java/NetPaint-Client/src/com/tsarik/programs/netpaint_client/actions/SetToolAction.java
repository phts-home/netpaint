package com.tsarik.programs.netpaint_client.actions;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import com.tsarik.programs.netpaint_client.components.*;
import com.tsarik.programs.netpaint_client.frames.MainFrame;

public class SetToolAction extends AbstractProgramMenuAction {
	
	public SetToolAction(MainFrame owner, String title, int tool) {
		super(owner, title);
		this.tool = tool;
		switch (tool) {
		case DrawingArea.TOOL_PEN:
			this.putValue(Action.SMALL_ICON, new ImageIcon("res/pen.gif"));
			break;
		case DrawingArea.TOOL_RECT:
			this.putValue(Action.SMALL_ICON, new ImageIcon("res/rect.gif"));
			break;
		case DrawingArea.TOOL_CIRCLE:
			this.putValue(Action.SMALL_ICON, new ImageIcon("res/circle.gif"));
			break;
		case DrawingArea.TOOL_ERASER:
			this.putValue(Action.SMALL_ICON, new ImageIcon("res/eraser.gif"));
			break;
		case DrawingArea.TOOL_FILLEDRECT:
			this.putValue(Action.SMALL_ICON, new ImageIcon("res/filledrect.gif"));
			break;
		case DrawingArea.TOOL_FILLEDCIRCLE:
			this.putValue(Action.SMALL_ICON, new ImageIcon("res/filledcircle.gif"));
			break;
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.getDrawingArea().setTool(tool);
	}
	
	private int tool;

}
