package com.tsarik.programs.netpaint_client.actions;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Icon;

class ColorIcon implements Icon {
	public ColorIcon(int w, int h, Color c) {
		this.w = w;
		this.h = h;
		this.color = c;
	}
	
	public void setColor(Color c) {
		this.color = c;
	}
	
	@Override
	public int getIconHeight() {
		return h;
	}
	
	@Override
	public int getIconWidth() {
		return w;
	}
	
	@Override
	public void paintIcon(Component c, Graphics g, int x, int y) {
		g.setColor(color);
		g.fillRect(0, 0, w+10, h+10);
	}
	
	int w;
	int h;
	Color color;
}
