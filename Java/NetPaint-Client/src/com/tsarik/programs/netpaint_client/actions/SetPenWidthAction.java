package com.tsarik.programs.netpaint_client.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.ImageIcon;

import com.tsarik.programs.netpaint_client.components.DrawingArea;
import com.tsarik.programs.netpaint_client.frames.MainFrame;

public class SetPenWidthAction extends AbstractProgramMenuAction {
	
	public SetPenWidthAction(MainFrame owner, String title, int penWidth) {
		super(owner, title);
		this.penWidth = penWidth;
		switch (penWidth) {
		case 1:
			this.putValue(Action.SMALL_ICON, new ImageIcon("res/thin.gif"));
			break;
		case 2:
			this.putValue(Action.SMALL_ICON, new ImageIcon("res/normal.gif"));
			break;
		case 3:
			this.putValue(Action.SMALL_ICON, new ImageIcon("res/thick.gif"));
			break;
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.getDrawingArea().setPenWidth(penWidth);
	}
	
	private int penWidth;

}
