package com.tsarik.programs.netpaint_client.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;

import com.tsarik.programs.netpaint_client.Parameters;
import com.tsarik.programs.netpaint_client.frames.MainFrame;
import com.tsarik.programs.netpaint_client.lib.ConnectionManager;

public class ClearAction extends AbstractProgramMenuAction {
	
	public ClearAction(MainFrame owner) {
		super(owner, "Clear", new ImageIcon("res/clear.gif"));
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.getDrawingArea().sendClear();
	}

}
