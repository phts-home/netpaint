package com.tsarik.programs.netpaint_client.actions;

import javax.swing.*;

import com.tsarik.programs.netpaint_client.frames.MainFrame;

public abstract class AbstractProgramMenuAction extends AbstractAction {
	
	public AbstractProgramMenuAction(String name) {
		super(name);
		this.owner = null;
	}
	
	public AbstractProgramMenuAction(MainFrame owner, String name) {
		super(name);
		this.owner = owner;
	}
	
	public AbstractProgramMenuAction(MainFrame owner, String name, Icon icon) {
		super(name, icon);
		this.owner = owner;
	}
	
	protected MainFrame owner;

}
