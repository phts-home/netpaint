package com.tsarik.programs.netpaint_client.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

import com.tsarik.programs.netpaint_client.Parameters;
import com.tsarik.programs.netpaint_client.frames.MainFrame;
import com.tsarik.programs.netpaint_client.lib.ConnectionManager;

public class LoadImageAction extends AbstractProgramMenuAction {
	
	public LoadImageAction(MainFrame owner) {
		super(owner, "Load Image...");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.loadImage();
	}

}
