package com.tsarik.programs.netpaint_client.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.KeyStroke;

import com.tsarik.programs.netpaint_client.Parameters;
import com.tsarik.programs.netpaint_client.frames.MainFrame;
import com.tsarik.programs.netpaint_client.lib.ConnectionManager;

public class CloseConnectionAction extends AbstractProgramMenuAction {
	
	public CloseConnectionAction(MainFrame owner) {
		super(owner, "Close Connection", new ImageIcon("res/disconnect.gif"));
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0));
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		try {
			switch (ConnectionManager.getInstance().closeConnection()) {
			case 5:	// CLIENTSOCKET_CLOSED
				//
				break;
			case 6:	// CLIENTSOCKET_ALREADY_CLOSED
				Parameters.showErrorMessage(owner, Parameters.MESSAGE_ALREADY_CLOSED);
				break;
			}
		} catch (UnsatisfiedLinkError e) {
			Parameters.showErrorMessage(owner, Parameters.MESSAGE_DLL_ERROR, e.getMessage());
		}
	}

}
