package com.tsarik.programs.netpaint_client.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.netpaint_client.frames.MainFrame;

public class SaveImageAction extends AbstractProgramMenuAction {
	
	public SaveImageAction(MainFrame owner) {
		super(owner, "Save Image As...");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.saveImage();
	}

}
