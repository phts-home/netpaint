package com.tsarik.programs.netpaint_client.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import com.tsarik.programs.netpaint_client.Parameters;
import com.tsarik.programs.netpaint_client.frames.MainFrame;
import com.tsarik.programs.netpaint_client.lib.ConnectionManager;

public class ConnectToServerAction extends AbstractProgramMenuAction {
	
	public ConnectToServerAction(MainFrame owner) {
		super(owner, "Connect To Server", new ImageIcon("res/connect.gif"));
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0));
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		try {
			switch(ConnectionManager.getInstance().connectToServer()) {
			case 0:	// CLIENTSOCKET_OK
				// nothing
				break;
			case 1:	// CLIENTSOCKET_ALREADY_CREATED
				Parameters.showErrorMessage(owner, Parameters.MESSAGE_CLIENT_ALREADYCREATED);
				break;
			case 10: // CLIENTSOCKET_ALREADY_CONNECTED
				Parameters.showErrorMessage(owner, Parameters.MESSAGE_CLIENT_ALREADYCONNECTED);
				break;
			case 2: // CLIENTSOCKET_WSA_FAILED
			case 3: // CLIENTSOCKET_CREATE_FAILED
				Parameters.showErrorMessage(owner, Parameters.MESSAGE_SOCKET_FAILED);
				break;
			case 4: // CLIENTSOCKET_CONNECT_FAILED
				Parameters.showErrorMessage(owner, Parameters.MESSAGE_CONNECTION_FAILED);
				break;
			}
		} catch (UnsatisfiedLinkError e) {
			Parameters.showErrorMessage(owner, Parameters.MESSAGE_DLL_ERROR, e.getMessage());
		}
	}
	
}
