package com.tsarik.programs.netpaint_client.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;

import com.tsarik.programs.netpaint_client.frames.MainFrame;

public class SetColorAction extends AbstractProgramMenuAction {
	
	public SetColorAction(MainFrame owner, String title, Color color) {
		super(owner, title, new ColorIcon(16, 16, color));
		this.color = color;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.getDrawingArea().setColor(color);
	}
	
	private Color color;

}
