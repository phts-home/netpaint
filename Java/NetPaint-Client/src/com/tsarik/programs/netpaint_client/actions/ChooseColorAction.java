package com.tsarik.programs.netpaint_client.actions;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.*;

import com.tsarik.components.event.*;

import com.tsarik.programs.netpaint_client.frames.*;

public class ChooseColorAction extends AbstractProgramMenuAction implements DrawingPanelListener {
	
	public ChooseColorAction(MainFrame owner) {
		super(owner, "Choose Color...", new ColorIcon(16, 16, Color.RED));
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		ColorChooserDialog c = owner.getColorChooser();
		c.setColor(owner.getDrawingArea().getColor());
		c.setVisible(true);
	}
	
	@Override
	public void stateChanged(DrawingPanelEvent evt) {
		((ColorIcon)this.getValue(Action.SMALL_ICON)).setColor(evt.getColor());
		owner.repaintToolBar();
	}
	
}
