package com.tsarik.programs.netpaint_client.lib;

import com.tsarik.programs.netpaint_client.*;


public class FromJavaToNativeInterface {
	
	/**
	 * Must be called within application initialization
	 */
	public static void init() throws UnsatisfiedLinkError {
		System.loadLibrary("ClientConnection");
	}
	
	public static native int connectToServer(String host, int port, String name);
	
	public static native int closeConnection();
	
	public static native boolean isCreated();
	
	public static native boolean isConnected();
	
	public static native int receiveMessagesFromServer();
	
	public static native int sendImageData(int imageWidth, int imageHeight, byte imageBytes[]);
	
	public static native int sendRequestedImageData(int imageWidth, int imageHeight, byte imageBytes[]);
	
	public static native int sendImageDimensionData(int imageWidth, int imageHeight);
	
	public static native int sendClearData();
	
	/*
	 * coords[0] = x1, coords[1] = y1, coords[2] = x2, coords[3] = y2 ...
	 */
	public static native int sendPenData(int[] coords, int red, int green, int blue, int penWidth);
	
	public static native int sendRectData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth);
	
	public static native int sendCircleData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth);
	
	public static native int sendEraserData(int coords[], int penWidth);
	
}
