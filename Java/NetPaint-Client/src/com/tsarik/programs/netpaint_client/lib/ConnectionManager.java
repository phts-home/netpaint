package com.tsarik.programs.netpaint_client.lib;

import com.tsarik.programs.netpaint_client.*;


public class ConnectionManager {
	
	private ConnectionManager(NetPaintClient application) {
		this.application = application;
	}
	
	private static ConnectionManager instance;
	
	public static void createInstance(NetPaintClient owner) {
		if (instance == null) {
			instance = new ConnectionManager(owner);
		}
	}
	
	public static synchronized ConnectionManager getInstance() {
		return instance;
	}
	
	
	
	public int closeConnection() throws UnsatisfiedLinkError {
		return FromJavaToNativeInterface.closeConnection();
	}
	
	public int connectToServer() throws UnsatisfiedLinkError {
		return connectToServer(Parameters.CON_SERVERHOST, Parameters.CON_PORT, Parameters.CON_NAME);
	}
	
	
	
	
	public void sendImageData(int imageWidth, int imageHeight, byte imageBytes[]) {
		try {
			FromJavaToNativeInterface.sendImageData(imageWidth, imageHeight, imageBytes);
		} catch (UnsatisfiedLinkError e) {
		}
	}
	
	public void sendRequestedImageData(int imageWidth, int imageHeight, byte imageBytes[]) {
		try {
			FromJavaToNativeInterface.sendRequestedImageData(imageWidth, imageHeight, imageBytes);
		} catch (UnsatisfiedLinkError e) {
		}
	}
	
	public void sendImageDimensionData(int imageWidth, int imageHeight) {
		try {
			FromJavaToNativeInterface.sendImageDimensionData(imageWidth, imageHeight);
		} catch (UnsatisfiedLinkError e) {
		}
	}
	
	
	
	
	
	public void sendClearData() {
		if (!isClientConnected()) {
			return;
		}
		try {
			FromJavaToNativeInterface.sendClearData();
		} catch (UnsatisfiedLinkError e) {
		}
	}
	
	public void sendPenData(int[] _coords, int coordsLength, int red, int green, int blue, int penWidth) {
		if (!isClientConnected()) {
			return;
		}
		try {
			int coords[] = new int [coordsLength];
			for (int i = 0; i < coordsLength; i++) {
				coords[i] = _coords[i];
			}
			FromJavaToNativeInterface.sendPenData(coords, red, green, blue, penWidth);
		} catch (UnsatisfiedLinkError e) {
		}
	}
	
	public void sendPenData(int[] coords, int red, int green, int blue, int penWidth) {
		if (!isClientConnected()) {
			return;
		}
		try {
			FromJavaToNativeInterface.sendPenData(coords, red, green, blue, penWidth);
		} catch (UnsatisfiedLinkError e) {
		}
	}
	
	public void sendRectData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth) {
		if (!isClientConnected()) {
			return;
		}
		try {
			FromJavaToNativeInterface.sendRectData(x1, y1, x2, y2, red, green, blue, penWidth);
		} catch (UnsatisfiedLinkError e) {
		}
	}
	
	public void sendCircleData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth) {
		if (!isClientConnected()) {
			return;
		}
		try {
			FromJavaToNativeInterface.sendCircleData(x1, y1, x2, y2, red, green, blue, penWidth);
		} catch (UnsatisfiedLinkError e) {
		}
	}
	
	public void sendEraserData(int[] _coords, int coordsLength, int penWidth) {
		if (!isClientConnected()) {
			return;
		}
		try {
			int coords[] = new int [coordsLength];
			for (int i = 0; i < coordsLength; i++) {
				coords[i] = _coords[i];
			}
			FromJavaToNativeInterface.sendEraserData(coords, penWidth);
		} catch (UnsatisfiedLinkError e) {
		}
	}
	
	public void sendEraserData(int[] coords, int penWidth) {
		if (!isClientConnected()) {
			return;
		}
		try {
			FromJavaToNativeInterface.sendEraserData(coords, penWidth);
		} catch (UnsatisfiedLinkError e) {
		}
	}
	
	
	
	
	public boolean isConnectionCreated() throws UnsatisfiedLinkError {
		return FromJavaToNativeInterface.isCreated();
	}
	
	public boolean isClientConnected() throws UnsatisfiedLinkError {
		return FromJavaToNativeInterface.isConnected();
	}
	
	
	private NetPaintClient application;
	
	private MessageRecieverFromServer receiverFromServer;
	
	
	private int connectToServer(String host, int port, String name) throws UnsatisfiedLinkError {
		if (host == null) {
			return -1;
		}
		
		int res = FromJavaToNativeInterface.connectToServer(host, port, name);
		if (res == 0) {	// CLIENTSOCKET_OK
			receiverFromServer = new MessageRecieverFromServer();
			receiverFromServer.start();
		}
		return res;
	}
	
	private class MessageRecieverFromServer extends Thread {
		@Override
		public void run() {
			try {
				FromJavaToNativeInterface.receiveMessagesFromServer();
			} catch (UnsatisfiedLinkError e) {
			}
		}
	}
	
	
}
