package com.tsarik.programs.netpaint_client.lib;


import java.io.IOException;

import javax.swing.DefaultListModel;

import com.tsarik.programs.netpaint_client.*;


public class FromNativeToJavaInterface {
	
	/**
	 * Must be called within application initialization
	 * 
	 * @param _application Application instance
	 */
	public static void init(NetPaintClient _application) {
		application = _application;
	}
	
	public static void receiveClientRefusedData() {
		Parameters.showErrorMessage(application.getMainFrame(), Parameters.MESSAGE_SERVER_FULL);
	}
	
	public static void receiveRequestImageData() {
		application.getMainFrame().getDrawingArea().sendRequestedImageData();
	}
	
	
	public static void receiveImageData(int imageWidth, int imageHeight, byte[] imageBytes) {
		try {
			Parameters.IMAGE_WIDTH = imageWidth;
			Parameters.IMAGE_HEIGHT = imageHeight;
			application.getMainFrame().getDrawingArea().setImageData(imageBytes);
		} catch (IOException e) {
		}
		if (application.getMainFrame().getDrawingArea().getImage() == null) {
			application.getMainFrame().getDrawingArea().setCanvasSize(imageWidth, imageHeight);
		}
	}
	
	public static void receiveImageDimensionData(int imageWidth, int imageHeight) {
		application.getMainFrame().getDrawingArea().setCanvasSize(imageWidth, imageHeight);
	}
	
	public static void receiveRefreshData(Object names[]) {
		for (int i = 0; i < names.length; i++) {
		}
		((DefaultListModel)application.getMainFrame().getClientList().getModel()).removeAllElements();
		if (names.length > 0) {
			names[0] = "[owner] ".concat((String)names[0]);
		}
		for (int i = 0; i < names.length; i++) {
			((DefaultListModel)application.getMainFrame().getClientList().getModel()).addElement(names[i]);
		}
	}
	
	
	
	public static void receiveClearData() {
		application.getMainFrame().getDrawingArea().clearImage();
	}
	
	public static void receiveUnableClearData() {
		Parameters.showErrorMessage(application.getMainFrame(), Parameters.MESSAGE_UNABLE_CLEAR);
	}
	
	public static void receivePenData(int coords[], int red, int green, int blue, int penWidth) {
		application.getMainFrame().getDrawingArea().drawPen(coords, red, green, blue, penWidth);
	}
	
	public static void receiveRectData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth) {
		application.getMainFrame().getDrawingArea().drawRect(x1, y1, x2, y2, red, green, blue, penWidth, penWidth == -1);
	}
	
	public static void receiveCircleData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth) {
		application.getMainFrame().getDrawingArea().drawCircle(x1, y1, x2, y2, red, green, blue, penWidth, penWidth == -1);
	}
	
	public static void receiveEraserData(int coords[], int penWidth) {
		application.getMainFrame().getDrawingArea().drawEraser(coords, penWidth);
	}
	
	public static void receiveUnableResizeData() {
		Parameters.showErrorMessage(application.getMainFrame(), Parameters.MESSAGE_UNABLE_RESIZE);
	}
	
	public static void receiveUnableLoadData() {
		Parameters.showErrorMessage(application.getMainFrame(), Parameters.MESSAGE_UNABLE_LOADIMAGE);
	}
	
	
	
	public static void closeConnection() {
		((DefaultListModel)application.getMainFrame().getClientList().getModel()).removeAllElements();
	}
	
	public static void lostConnection() {
		((DefaultListModel)application.getMainFrame().getClientList().getModel()).removeAllElements();
		Parameters.showErrorMessage(application.getMainFrame(), Parameters.MESSAGE_CONNECTION_LOST);
	}
	
	
	
	private static NetPaintClient application;
	
}
