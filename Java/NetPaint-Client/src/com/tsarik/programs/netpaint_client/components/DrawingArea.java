package com.tsarik.programs.netpaint_client.components;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EtchedBorder;

import com.tsarik.components.DrawingPanel;
import com.tsarik.programs.netpaint_client.*;
import com.tsarik.programs.netpaint_client.actions.*;
import com.tsarik.programs.netpaint_client.frames.*;
import com.tsarik.programs.netpaint_client.lib.ConnectionManager;


public class DrawingArea extends DrawingPanel {
	
	public DrawingArea(MainFrame owner) {
		super();
		MouseAdapter mouseHandler = new MouseHandler();
		addMouseListener(mouseHandler);
		addMouseMotionListener(mouseHandler);
		this.setLayout(null);
		
		cleared = false;
		
		popup = new JPopupMenu();
		popup.add(new SetColorAction(owner, "White", Color.WHITE));
		popup.add(new SetColorAction(owner, "Light Gray", Color.LIGHT_GRAY));
		popup.add(new SetColorAction(owner, "Gray", Color.GRAY));
		popup.add(new SetColorAction(owner, "Black", Color.BLACK));
		popup.addSeparator();
		popup.add(new SetColorAction(owner, "Red", Color.RED));
		popup.add(new SetColorAction(owner, "Green", Color.GREEN));
		popup.add(new SetColorAction(owner, "Blue", Color.BLUE));
		popup.add(new SetColorAction(owner, "Cyan", Color.CYAN));
		popup.add(new SetColorAction(owner, "Magenta", Color.MAGENTA));
		popup.add(new SetColorAction(owner, "Yellow", Color.YELLOW));
		popup.addSeparator();
		popup.add(new SetColorAction(owner, "Orange", Color.ORANGE));
		popup.add(new SetColorAction(owner, "Brown", new Color(128, 64, 0)));
		popup.add(new SetColorAction(owner, "Crimson", new Color(128, 0, 0)));
		popup.add(new SetColorAction(owner, "Pink", Color.PINK));
		popup.add(new SetColorAction(owner, "Sky-blue", new Color(192, 255, 255)));
		popup.add(new SetColorAction(owner, "Purple", new Color(128, 0, 255)));
	}
	
	@Override
	public void setCanvasSize(int canvasWidth, int canvasHeight) {
		super.setCanvasSize(canvasWidth, canvasHeight);
		setPreferredSize(new Dimension(canvasWidth, canvasHeight));
	}
	
	@Override
	public void setImage(BufferedImage image) {
		if (image == null) {
			return;
		}
		Parameters.IMAGE_WIDTH = image.getWidth();
		Parameters.IMAGE_HEIGHT = image.getHeight();
		super.setImage(image);
		setPreferredSize(new Dimension(getCanvasWidth(), getCanvasHeight()));
	}
	
	@Override
	public void setImageData(byte[] data) throws IOException {
		if (data == null) {
			return;
		}
		
		super.setImageData(data);
		setPreferredSize(new Dimension(getCanvasWidth(), getCanvasHeight()));
	}
	
	@Override
	public void clearImage() {
		cleared = true;
		super.clearImage();
	}
	
	public void sendImage(BufferedImage image) {
		if (image == null) {
			return;
		}
		if (ConnectionManager.getInstance().isClientConnected()) {
			try {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write(image, "bmp", baos);
				byte[] data =  baos.toByteArray();
				sendImageData(image.getWidth(), image.getHeight(), data);
			} catch (IOException e) {
			}
		} else {
			setImage(image);
		}
	}
	
	public void sendRequestedImageData() {
		try {
			ConnectionManager.getInstance().sendRequestedImageData(Parameters.IMAGE_WIDTH, Parameters.IMAGE_HEIGHT, this.getImageData());
		} catch (IOException e) {
		}
	}
	
	public void sendClear() {
		if (ConnectionManager.getInstance().isClientConnected()) {
			ConnectionManager.getInstance().sendClearData();
		} else {
			clearImage();
		}
	}
	
	
	private JPopupMenu popup;
	private boolean cleared;
	
	
	private void sendImageData(int w, int h, byte[] data) {
		ConnectionManager.getInstance().sendImageData(w, h, data);
	}
	
	private class MouseHandler extends MouseAdapter {
		
		private final int MAXSIZE = 100;
		
		public MouseHandler() {
			left = false;
			canceled = false;
			x = 0;
			y = 0;
			coordinates = new int[MAXSIZE];
			coordLength = 0;
		}
		
		@Override
		public void mousePressed(MouseEvent evt) {
			if (left) {
				canceled = true;
				canvas.clearSecondImage();
				canvas.repaint();
				left = false;
				return;
			}
			left = evt.getButton() == 1;
			if (!left) {
				return;
			}
			if (cleared) {
				cleared = false;
			}
			x = evt.getX();
			y = evt.getY();
			switch (tool) {
			case TOOL_PEN:
			case TOOL_ERASER:
				coordinates[coordLength] = x;
				coordLength++;
				coordinates[coordLength] = y;
				coordLength++;
				
				drawLine(x, y, x, y, penWidth);
				canvas.repaint();
				break;
			case TOOL_RECT:
			case TOOL_CIRCLE:
			case TOOL_FILLEDRECT:
			case TOOL_FILLEDCIRCLE:
				// nothing
				break;
			}
		}
		
		@Override
		public void mouseReleased(MouseEvent evt) {
			if (!canceled && evt.isPopupTrigger()) {
				popup.show(DrawingArea.this, evt.getX(), evt.getY());
			}
			canceled = false;
			if (cleared) {
				cleared = false;
				return;
			}
			if (!left) {
				return;
			}
			left = false;
			switch (tool) {
			case TOOL_PEN:
				coordinates[coordLength] = x;
				coordLength++;
				coordinates[coordLength] = y;
				coordLength++;
				ConnectionManager.getInstance().sendPenData(coordinates, coordLength, color.getRed(), color.getGreen(), color.getBlue(), penWidth);
				coordLength = 0;
				canvas.repaint();
				break;
			case TOOL_RECT:
				canvas.clearSecondImage();
				drawRect(x, y, evt.getX(), evt.getY(), penWidth);
				canvas.repaint();
				ConnectionManager.getInstance().sendRectData(x, y, evt.getX(), evt.getY(), color.getRed(), color.getGreen(), color.getBlue(), penWidth);
				break;
			case TOOL_CIRCLE:
				canvas.clearSecondImage();
				drawCircle(x, y, evt.getX(), evt.getY(), penWidth);
				canvas.repaint();
				ConnectionManager.getInstance().sendCircleData(x, y, evt.getX(), evt.getY(), color.getRed(), color.getGreen(), color.getBlue(), penWidth);
				break;
			case TOOL_FILLEDRECT:
				canvas.clearSecondImage();
				fillRect(x, y, evt.getX(), evt.getY());
				canvas.repaint();
				ConnectionManager.getInstance().sendRectData(x, y, evt.getX(), evt.getY(), color.getRed(), color.getGreen(), color.getBlue(), -1);
				break;
			case TOOL_FILLEDCIRCLE:
				canvas.clearSecondImage();
				fillCircle(x, y, evt.getX(), evt.getY());
				canvas.repaint();
				ConnectionManager.getInstance().sendCircleData(x, y, evt.getX(), evt.getY(), color.getRed(), color.getGreen(), color.getBlue(), -1);
				break;
			case TOOL_ERASER:
				coordinates[coordLength] = x;
				coordLength++;
				coordinates[coordLength] = y;
				coordLength++;
				ConnectionManager.getInstance().sendEraserData(coordinates, coordLength, penWidth);
				coordLength = 0;
				canvas.repaint();
				break;
			}
		}
		
		@Override
		public void mouseDragged(MouseEvent evt) {
			if (canceled) {
				return;
			}
			if (!left) {
				return;
			}
			if (cleared) {
				return;
			}
			switch (tool) {
			case TOOL_PEN:
				coordinates[coordLength] = x;
				coordLength++;
				coordinates[coordLength] = y;
				coordLength++;
				
				if (coordLength == MAXSIZE) {
					ConnectionManager.getInstance().sendPenData(coordinates, color.getRed(), color.getGreen(), color.getBlue(), penWidth);
					coordLength = 2;
					coordinates[0] = x;
					coordinates[1] = y;
				}
				
				drawLine(x, y, evt.getX(), evt.getY(), penWidth);
				x = evt.getX();
				y = evt.getY();
				canvas.repaint();
				break;
			case TOOL_RECT:
				canvas.clearSecondImage();
				drawRect(x, y, evt.getX(), evt.getY(), penWidth, canvas.getSecondImageGraphics());
				canvas.repaint();
				break;
			case TOOL_CIRCLE:
				canvas.clearSecondImage();
				drawCircle(x, y, evt.getX(), evt.getY(), penWidth, canvas.getSecondImageGraphics());
				canvas.repaint();
				break;
			case TOOL_FILLEDRECT:
				canvas.clearSecondImage();
				fillRect(x, y, evt.getX(), evt.getY(), canvas.getSecondImageGraphics());
				canvas.repaint();
				break;
			case TOOL_FILLEDCIRCLE:
				canvas.clearSecondImage();
				fillCircle(x, y, evt.getX(), evt.getY(), canvas.getSecondImageGraphics());
				canvas.repaint();
				break;
			case TOOL_ERASER:
				coordinates[coordLength] = x;
				coordLength++;
				coordinates[coordLength] = y;
				coordLength++;
				
				if (coordLength == MAXSIZE) {
					ConnectionManager.getInstance().sendEraserData(coordinates, penWidth);
					coordLength = 2;
					coordinates[0] = x;
					coordinates[1] = y;
				}
				
				drawLine(x, y, evt.getX(), evt.getY(), penWidth);
				x = evt.getX();
				y = evt.getY();
				canvas.repaint();
				break;
			}
			
		}
		
		private boolean left;
		private boolean canceled;
		private int x;
		private int y;
		
		private int[] coordinates;
		private int coordLength;
	}
	
}
