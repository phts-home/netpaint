#pragma once

#include <jni.h>


class FromNativeToJavaInterface
{
public:
	FromNativeToJavaInterface();
	~FromNativeToJavaInterface();

public:
	static void receiveClientRefusedData(JNIEnv * env);
	static void receiveRequestImageData(JNIEnv *env);
	static void receiveImageData(JNIEnv *env, int _imageWidth, int _imageHeight, char *_imageBytesBuf, int _arraySize);
	static void receiveImageDimensionData(JNIEnv *env, int _imageWidth, int _imageHeight);

	static void receiveRefreshData(JNIEnv *env, char **names, int arraysize);
	
	static void receiveClearData(JNIEnv *env);
	static void receiveUnableClearData(JNIEnv *env);
	static void receivePenData(JNIEnv *env, int *_coords, int coordsLength, int red, int green, int blue, int penWidth);
	static void receiveRectData(JNIEnv *env, int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth);
	static void receiveCircleData(JNIEnv *env, int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth);
	static void receiveEraserData(JNIEnv *env, int *_coords, int coordsLength, int penWidth);
	static void receiveUnableResizeData(JNIEnv *env);
	static void receiveUnableLoadData(JNIEnv *env);

	static void closeConnection(JNIEnv *env);
	static void lostConnection(JNIEnv *env);
};
