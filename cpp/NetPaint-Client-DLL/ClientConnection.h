#pragma once

#include "Winsock2.h"

#include "FromNativeToJavaInterface.h"
#include "..\\NetPaint-Server\SendingData.h"

#define CLIENTSOCKET_OK 0
#define CLIENTSOCKET_ALREADY_CREATED 1
#define CLIENTSOCKET_WSA_FAILED 2
#define CLIENTSOCKET_CREATE_FAILED 3
#define CLIENTSOCKET_CONNECT_FAILED 4
#define CLIENTSOCKET_CLOSED 5
#define CLIENTSOCKET_ALREADY_CLOSED 6
#define CLIENTSOCKET_HOST_NOTFOUND 8
#define CLIENTSOCKET_WRONG_ADDRESS 9
#define CLIENTSOCKET_ALREADY_CONNECTED 10

#define WAIT_PERIOD 60000


/*
 * Describes client connection to the program server
 */
class ClientConnection
{
public:
	//
	// Tries to connect to the program server
	// Returns CLIENTSOCKET_OK if connection is successfully
	//
	static int create(const char *host, int port);

	//
	// Tests whether the connection is created
	// Returns true if connection was created successfully or otherwise false
	//
	static bool isCreated();

	//
	// Tests whether the client is connected to the server
	// Returns true if client is connected or otherwise false
	//
	static bool isConnected();

	//
	// This block of methods sends the specefied type of data to the server
	//
	static int sendRequestToConnect(const char *name);
	static int sendRequestToClose();
	static int sendRequestToRefresh();
	static int sendImageData(bool requested, int imageWidth, int imageHeight, char *imageBytes, int arraySize);
	static int sendImageDimensionData(int imageWidth, int imageHeight);
	static int sendClearData();
	static int sendPenData(int *coords, int coordsLength, int red, int green, int blue, int penWidth);
	static int sendRectData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth);
	static int sendCircleData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth);
	static int sendEraserData(int *coords, int coordsLength, int penWidth);

	//
	// Receives messages from server. Called by Java side
	//
	static int receiveMessagesFromServer(JNIEnv *env);

private:
	static SOCKET clientSocket;
	static bool created;
	static bool connected;

	static int close();
	static int sendDataToServer(SendingData *data);

	static DWORD WINAPI waitForConnect(PVOID pParam);
};