#include "StdAfx.h"

#include "ClientConnection.h"

#include "stdlib.h"

#pragma comment(lib, "Ws2_32.lib")



//////////////////////////////////////////////////////////////////////////
// private parameters
//

SOCKET ClientConnection::clientSocket = NULL;

bool ClientConnection::created = false;

bool ClientConnection::connected = false;




//////////////////////////////////////////////////////////////////////////
// public methods
//

int ClientConnection::create(const char *host, int port)
{
	if (connected) {
		return CLIENTSOCKET_ALREADY_CONNECTED;
	}

	if (created) {
		return CLIENTSOCKET_ALREADY_CREATED;
	}

	WSADATA wsd;

	if (WSAStartup(MAKEWORD(2,2), &wsd) != 0) {
		return CLIENTSOCKET_WSA_FAILED;
	}
 
	struct sockaddr_in peer;
	int rc;

	hostent* remoteHost = gethostbyname(host);
	if (WSAGetLastError() != 0) {
		if (WSAGetLastError() == 11001) {
			return CLIENTSOCKET_HOST_NOTFOUND;
		} else {
			return -2;
		}
	}
	peer.sin_addr.s_addr = inet_addr(inet_ntoa(*(struct in_addr *)remoteHost->h_addr_list[0]));
	peer.sin_family = AF_INET;
	peer.sin_port = htons(port);

	clientSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (clientSocket == INVALID_SOCKET) {
		clientSocket = NULL;
		WSACleanup();
		return CLIENTSOCKET_CREATE_FAILED;
	}

	rc = connect(clientSocket, (struct sockaddr *)&peer, sizeof(peer));
	if (rc == SOCKET_ERROR) {
		clientSocket = NULL;
		WSACleanup();
		return CLIENTSOCKET_CONNECT_FAILED;
	}

	created = true;
	
	return CLIENTSOCKET_OK;
}

int ClientConnection::sendRequestToConnect(const char *name)
{
	RequestClientConnectData *requestClientConnectData = new RequestClientConnectData(name);
	sendDataToServer(requestClientConnectData);
	delete requestClientConnectData;

	CreateThread(NULL, 0, waitForConnect, NULL, 0, NULL);

	return 0;
}

int ClientConnection::sendRequestToClose()
{
	if ( ! isCreated() ) {
		return CLIENTSOCKET_ALREADY_CLOSED;
	}
	if ( ! isConnected() ) {
		ClientConnection::close();
		return CLIENTSOCKET_OK;
	}
	RequestClientCloseData *requestClientCloseData = new RequestClientCloseData();
	sendDataToServer(requestClientCloseData);
	delete requestClientCloseData;
	return CLIENTSOCKET_OK;
}

int ClientConnection::sendRequestToRefresh()
{
	if ( ! isConnected() ) {
		return -1;
	}
	RequestRefreshData *requestRefreshData = new RequestRefreshData();
	sendDataToServer(requestRefreshData);
	delete requestRefreshData;
	return 0;
}

bool ClientConnection::isCreated()
{
	return created;
}

bool ClientConnection::isConnected()
{
	return connected;
}




int ClientConnection::sendImageData(bool requested, int imageWidth, int imageHeight, char *imageBytes, int arraySize)
{
	ImageData *imageData = new ImageData(requested, imageWidth, imageHeight, imageBytes, arraySize);
	int res = imageData->sendData(clientSocket);
	delete imageData;
	return res;
}

int ClientConnection::sendImageDimensionData(int imageWidth, int imageHeight)
{
	ImageDimensionData *imageDimensionData = new ImageDimensionData(imageWidth, imageHeight);
	int res = imageDimensionData->sendData(clientSocket);
	delete imageDimensionData;
	return res;
}

int ClientConnection::sendClearData()
{
	ClearData *clearData = new ClearData();
	sendDataToServer(clearData);
	delete clearData;
	return 0;
}

int ClientConnection::sendPenData(int *coords, int coordsLength, int red, int green, int blue, int penWidth)
{
	PenData *penData = new PenData(coords, coordsLength, red, green, blue, penWidth);
	sendDataToServer(penData);
	delete penData;
	return 0;
}

int ClientConnection::sendRectData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth)
{
	RectData *rectData = new RectData(x1, y1, x2, y2, red, green, blue, penWidth);
	sendDataToServer(rectData);
	delete rectData;
	return 0;
}

int ClientConnection::sendCircleData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth)
{
	CircleData *circleData = new CircleData(x1, y1, x2, y2, red, green, blue, penWidth);
	sendDataToServer(circleData);
	delete circleData;
	return 0;
}

int ClientConnection::sendEraserData(int *coords, int coordsLength, int penWidth)
{
	EraserData *eraserData = new EraserData(coords, coordsLength, penWidth);
	sendDataToServer(eraserData);
	delete eraserData;
	return 0;
}




int ClientConnection::receiveMessagesFromServer(JNIEnv *env)
{
	char buf[sizeof(int)];
	int received;
	while (TRUE) {
		received = recv(clientSocket, buf, sizeof(buf), 0);
		if (received <= 0) {
			ClientConnection::close();
			FromNativeToJavaInterface::lostConnection(env);
			return -1;
		}
		if (received != sizeof(buf)) {
			continue;
		}
		int* ptype = (int*)buf;
		int type = *ptype;
		switch (type) {
			case DATATYPE_CLIENT_REFUSED:
				{
					ClientConnection::close();
					FromNativeToJavaInterface::receiveClientRefusedData(env);
					return -3;
				}
			case DATATYPE_REQUEST_IMAGE_DATA:
				{
					FromNativeToJavaInterface::receiveRequestImageData(env);
					break;
				}
			case DATATYPE_IMAGE_DATA:
				{
					ImageData *imageData = ImageData::recieveData(clientSocket);
					FromNativeToJavaInterface::receiveImageData(env, imageData->getImageWidth(), imageData->getImageHeight(), imageData->getImageBytes(), imageData->getImageBytesLength());
					delete imageData;
					break;
				}
			case DATATYPE_IMAGE_DIMENSION_DATA:
				{
					ImageDimensionData *imageDimensionData = ImageDimensionData::recieveData(clientSocket);
					FromNativeToJavaInterface::receiveImageDimensionData(env, imageDimensionData->getImageWidth(), imageDimensionData->getImageHeight());
					delete imageDimensionData;
					break;
				}
			case DATATYPE_INIT:
				{
					connected = true;
					ClientConnection::sendRequestToRefresh();
					break;
				}
			case DATATYPE_REFRESH:
				{
					RefreshData *refreshData = RefreshData::recieveData(clientSocket);
					FromNativeToJavaInterface::receiveRefreshData(env, refreshData->getNames(), refreshData->getClientCount());
					delete refreshData;
					break;
				}
			case DATATYPE_CLEAR:
				{
					FromNativeToJavaInterface::receiveClearData(env);
					break;
				}
			case DATATYPE_UNABLE_CLEAR:
				{
					FromNativeToJavaInterface::receiveUnableClearData(env);
					break;
				}
			case DATATYPE_PEN:
				{
					PenData *penData = PenData::recieveData(clientSocket);
					FromNativeToJavaInterface::receivePenData(env, penData->getCoords(), penData->getCoordsLength(), penData->getRed(), penData->getGreen(), penData->getBlue(), penData->getPenWidth());
					delete penData;
					break;
				}
			case DATATYPE_RECT:
				{
					RectData *rectData = RectData::recieveData(clientSocket);
					FromNativeToJavaInterface::receiveRectData(env, rectData->getX1(), rectData->getY1(), rectData->getX2(), rectData->getY2(), rectData->getRed(), rectData->getGreen(), rectData->getBlue(), rectData->getPenWidth());
					delete rectData;
					break;
				}
			case DATATYPE_CIRCLE:
				{
					CircleData *circleData = CircleData::recieveData(clientSocket);
					FromNativeToJavaInterface::receiveCircleData(env, circleData->getX1(), circleData->getY1(), circleData->getX2(), circleData->getY2(), circleData->getRed(), circleData->getGreen(), circleData->getBlue(), circleData->getPenWidth());
					delete circleData;
					break;
				}
			case DATATYPE_ERASER:
				{
					EraserData *eraserData = EraserData::recieveData(clientSocket);
					FromNativeToJavaInterface::receiveEraserData(env, eraserData->getCoords(), eraserData->getCoordsLength(), eraserData->getPenWidth());
					delete eraserData;
					break;
				}
			case DATATYPE_UNABLE_RESIZE:
				{
					FromNativeToJavaInterface::receiveUnableResizeData(env);
					break;
				}
			case DATATYPE_UNABLE_LOAD:
				{
					FromNativeToJavaInterface::receiveUnableLoadData(env);
					break;
				}
			case DATATYPE_ACCEPT_CLIENT_CLOSE:
				{
					ClientConnection::close();
					FromNativeToJavaInterface::closeConnection(env);
					return 0;
				}
		}
	}
	return -4;
}




//////////////////////////////////////////////////////////////////////////
// private methods
//

int ClientConnection::close()
{
	if (clientSocket == NULL) {
		created = false;
		return CLIENTSOCKET_ALREADY_CLOSED;
	}

	closesocket(clientSocket);

	clientSocket = NULL;
	created = false;
	connected = false;

	WSACleanup();
	return CLIENTSOCKET_CLOSED;
}

int ClientConnection::sendDataToServer(SendingData *data)
{
	if (data == NULL) {
		return -1;
	}
	return data->sendData(clientSocket);
}

DWORD WINAPI ClientConnection::waitForConnect(PVOID pParam)
{
	Sleep(WAIT_PERIOD);
	if (!created) {
		return 0;
	}
	if (!connected) {
		ClientConnection::close();
	}
	return 0;
}
