#include "StdAfx.h"

#include "com_tsarik_programs_netpaint_client_lib_FromJavaToNativeInterface.h"

#include <stdlib.h>
#include <string.h>

#include "ClientConnection.h"
#include "..\\NetPaint-Server\SendingData.h"



JNIEXPORT jint JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_connectToServer
  (JNIEnv *env, jclass cls, jstring _host, jint _port, jstring _name)
{
	// convert host value from jstring to char*
	const char *host = env->GetStringUTFChars(_host, 0);
	
	// connect to server
	int connected = ClientConnection::create(host, _port);

	env->ReleaseStringUTFChars(_host, host);

	if (connected == CLIENTSOCKET_OK) {
		// send to server the REQUEST DATA
		const char *name = env->GetStringUTFChars(_name, 0);
		ClientConnection::sendRequestToConnect(name);
		env->ReleaseStringUTFChars(_name, name);
	}
	return connected;
}


JNIEXPORT jint JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_closeConnection
  (JNIEnv *, jclass)
{
	return ClientConnection::sendRequestToClose();
}

JNIEXPORT jboolean JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_isCreated
  (JNIEnv *, jclass)
{
	return ClientConnection::isCreated();
}

JNIEXPORT jboolean JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_isConnected
  (JNIEnv *, jclass)
{
	return ClientConnection::isConnected();
}

JNIEXPORT jint JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_receiveMessagesFromServer
  (JNIEnv *env, jclass)
{
	return ClientConnection::receiveMessagesFromServer(env);
}

JNIEXPORT jint JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_sendImageData
  (JNIEnv *env, jclass, jint _imageWidth, jint _imageHeight, jbyteArray _imageBytes)
{
	jbyte *__imageBytes = env->GetByteArrayElements(_imageBytes, 0);
	char *imageBytes = (char *)__imageBytes;
	int res = ClientConnection::sendImageData(false, _imageWidth, _imageHeight, imageBytes, env->GetArrayLength(_imageBytes));

	env->ReleaseByteArrayElements(_imageBytes, __imageBytes, 0);
	return 0;
}


JNIEXPORT jint JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_sendRequestedImageData
  (JNIEnv *env, jclass, jint _imageWidth, jint _imageHeight, jbyteArray _imageBytes)
{
	jbyte *__imageBytes = env->GetByteArrayElements(_imageBytes, 0);
	char *imageBytes = (char *)__imageBytes;
	int res = ClientConnection::sendImageData(true, _imageWidth, _imageHeight, imageBytes, env->GetArrayLength(_imageBytes));

	env->ReleaseByteArrayElements(_imageBytes, __imageBytes, 0);
	return 0;
}

JNIEXPORT jint JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_sendImageDimensionData
  (JNIEnv *env, jclass, jint _imageWidth, jint _imageHeight)
{
	ClientConnection::sendImageDimensionData(_imageWidth, _imageHeight);
	return 0;
}

JNIEXPORT jint JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_sendClearData
  (JNIEnv *, jclass)
{
	ClientConnection::sendClearData();
	return 0;
}

JNIEXPORT jint JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_sendPenData
  (JNIEnv *env, jclass, jintArray _coords, jint _red, jint _green, jint _blue, jint _penWidth)
{
	int coordsLength = env->GetArrayLength(_coords);

	jint *elements = env->GetIntArrayElements(_coords, 0);

	int *coords = (int *)calloc(coordsLength, sizeof(int));
	for (int i = 0; i < coordsLength; i++) {
		coords[i] = elements[i];
	}

	env->ReleaseIntArrayElements(_coords, elements, 0);

	ClientConnection::sendPenData(coords, coordsLength, _red, _green, _blue, _penWidth);
	return 0;
}


JNIEXPORT jint JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_sendRectData
  (JNIEnv *env, jclass, jint _x1, jint _y1, jint _x2, jint _y2, jint _red, jint _green, jint _blue, jint _penWidth)
{
	return ClientConnection::sendRectData(_x1, _y1, _x2, _y2, _red, _green, _blue, _penWidth);
}

JNIEXPORT jint JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_sendCircleData
  (JNIEnv *env, jclass, jint _x1, jint _y1, jint _x2, jint _y2, jint _red, jint _green, jint _blue, jint _penWidth)
{
	return ClientConnection::sendCircleData(_x1, _y1, _x2, _y2, _red, _green, _blue, _penWidth);
}

JNIEXPORT jint JNICALL Java_com_tsarik_programs_netpaint_1client_lib_FromJavaToNativeInterface_sendEraserData
  (JNIEnv *env, jclass, jintArray _coords, jint _penWidth)
{
	int coordsLength = env->GetArrayLength(_coords);

	jint *elements = env->GetIntArrayElements(_coords, 0);

	int *coords = (int *)calloc(coordsLength, sizeof(int));
	for (int i = 0; i < coordsLength; i++) {
		coords[i] = elements[i];
	}
	
	env->ReleaseIntArrayElements(_coords, elements, 0);

	ClientConnection::sendEraserData(coords, coordsLength, _penWidth);
	return 0;
}


