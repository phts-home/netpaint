#include "StdAfx.h"

#include "FromNativeToJavaInterface.h"





void FromNativeToJavaInterface::receiveClientRefusedData(JNIEnv *env)
{
	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receiveClientRefusedData", "()V");
	env->CallStaticVoidMethod(cls, mid);
}

void FromNativeToJavaInterface::receiveRequestImageData(JNIEnv *env)
{
	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receiveRequestImageData", "()V");
	env->CallStaticVoidMethod(cls, mid);
}

void FromNativeToJavaInterface::receiveImageData(JNIEnv *env, int _imageWidth, int _imageHeight, char *_imageBytesBuf, int _arraySize)
{
	if (_imageBytesBuf == NULL) {
		return;
	}
	jbyteArray imageBytes = env->NewByteArray(_arraySize);
	jbyte *bytes = env->GetByteArrayElements(imageBytes, 0);
	for (int i = 0; i < _arraySize; i++) {
		bytes[i] = _imageBytesBuf[i];
	}
	env->SetByteArrayRegion(imageBytes, 0, _arraySize, bytes);
	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receiveImageData", "(II[B)V");
	env->CallStaticVoidMethod(cls, mid, _imageWidth, _imageHeight, imageBytes);
	env->ReleaseByteArrayElements(imageBytes, bytes, 0);
}

void FromNativeToJavaInterface::receiveImageDimensionData(JNIEnv *env, int _imageWidth, int _imageHeight)
{
	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receiveImageDimensionData", "(II)V");
	env->CallStaticVoidMethod(cls, mid, _imageWidth, _imageHeight);
}

void FromNativeToJavaInterface::receiveRefreshData(JNIEnv *env, char **_names, int arraysize)
{
	jclass stringClass = env->FindClass("java/lang/String");
	jobjectArray names = env->NewObjectArray(arraysize, stringClass, NULL);

	for (int i = 0; i < arraysize; i++) {
		jstring el = env->NewStringUTF(_names[i]);
		env->SetObjectArrayElement(names, i, el);
	}

	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receiveRefreshData", "([Ljava/lang/Object;)V");
	env->CallStaticVoidMethod(cls, mid, names);
}



void FromNativeToJavaInterface::receiveClearData(JNIEnv* env)
{
	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receiveClearData", "()V");
	env->CallStaticVoidMethod(cls, mid);
}

void FromNativeToJavaInterface::receiveUnableClearData(JNIEnv* env)
{
	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receiveUnableClearData", "()V");
	env->CallStaticVoidMethod(cls, mid);
}

void FromNativeToJavaInterface::receivePenData(JNIEnv *env, int *_coords, int coordsLength, int red, int green, int blue, int penWidth)
{
	jintArray coords = env->NewIntArray(coordsLength);

	jint *elements = env->GetIntArrayElements(coords, 0);
	for (int i = 0; i < coordsLength; i++) {
		elements[i] = _coords[i];
	}
	env->SetIntArrayRegion(coords, 0, coordsLength, elements);

	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receivePenData", "([IIIII)V");
	env->CallStaticVoidMethod(cls, mid, coords, red, green, blue, penWidth);

	env->ReleaseIntArrayElements(coords, elements, 0);
}

void FromNativeToJavaInterface::receiveRectData(JNIEnv *env, int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth)
{
	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receiveRectData", "(IIIIIIII)V");
	env->CallStaticVoidMethod(cls, mid, x1, y1, x2, y2, red, green, blue, penWidth);
}

void FromNativeToJavaInterface::receiveCircleData(JNIEnv *env, int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth)
{
	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receiveCircleData", "(IIIIIIII)V");
	env->CallStaticVoidMethod(cls, mid, x1, y1, x2, y2, red, green, blue, penWidth);
}

void FromNativeToJavaInterface::receiveEraserData(JNIEnv *env, int *_coords, int coordsLength, int penWidth)
{
	jintArray coords = env->NewIntArray(coordsLength);

	jint *elements = env->GetIntArrayElements(coords, 0);
	for (int i = 0; i < coordsLength; i++) {
		elements[i] = _coords[i];
	}
	env->SetIntArrayRegion(coords, 0, coordsLength, elements);

	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receiveEraserData", "([II)V");
	env->CallStaticVoidMethod(cls, mid, coords, penWidth);

	env->ReleaseIntArrayElements(coords, elements, 0);
}

void FromNativeToJavaInterface::receiveUnableResizeData(JNIEnv* env)
{
	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receiveUnableResizeData", "()V");
	env->CallStaticVoidMethod(cls, mid);
}

void FromNativeToJavaInterface::receiveUnableLoadData(JNIEnv *env)
{
	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "receiveUnableLoadData", "()V");
	env->CallStaticVoidMethod(cls, mid);
}




void FromNativeToJavaInterface::closeConnection(JNIEnv *env)
{
	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "closeConnection", "()V");
	env->CallStaticVoidMethod(cls, mid);
}

void FromNativeToJavaInterface::lostConnection(JNIEnv *env)
{
	jclass cls = env->FindClass("com/tsarik/programs/netpaint_client/lib/FromNativeToJavaInterface");
	jmethodID mid = env->GetStaticMethodID(cls, "lostConnection", "()V");
	env->CallStaticVoidMethod(cls, mid);
}

