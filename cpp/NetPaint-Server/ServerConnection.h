#pragma once

#include "Winsock2.h"

#include "SendingData.h"
#include "Client.h"
#include "ClientList.h"


#define SERVERSOCKET_OK 0
#define SERVERSOCKET_ALREADY_CREATED 1
#define SERVERSOCKET_WSA_FAILED 2
#define SERVERSOCKET_CREATE_FAILED 3
#define SERVERSOCKET_BIND_FAILED 4
#define SERVERSOCKET_LISTEN_FAILED 5
#define SERVERSOCKET_CLOSED 6
#define SERVERSOCKET_ALREADY_CLOSED 7

#define CLIENTS_MAXCNT 20


/*
 * Describes program server connection
 */
class ServerConnection
{
public:
	//
	// Creates server connection
	// Returns SERVERSOCKET_OK if connection is successfully
	//
	static int create();

	//
	// Closes server connection
	//
	static int close();

private:
	static SOCKET serverSocket;
	static ClientList *clientList;
	static ClientList *waitingClients;
	static HANDLE observeClientsThreadHandle;
	static int newClientId;
	
	static void sendRefreshDataToClients();

	static DWORD WINAPI observeClientsThread(PVOID pParam);
	static DWORD WINAPI receiveRequestConnectThread(PVOID pParam);
	static DWORD WINAPI receiveMessagesFromClientThread(PVOID pParam);
};
