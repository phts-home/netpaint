#pragma once

#include "Winsock2.h"
#include "SendingData.h"

class Client
{
public:
	Client(SOCKET socket, int id, char *name);
	~Client();

public:
	int close();
	SOCKET getSocket();
	int getId() const;
	char* getName() const;
	void setId(int id);
	int recvFromClient(char *buf, int len);
	int sendData(SendingData *data);

private:
	SOCKET socket;
	int id;
	char *name;
};
