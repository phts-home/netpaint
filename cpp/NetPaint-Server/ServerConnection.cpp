#include "StdAfx.h"

#include "ServerConnection.h"

#include "stdlib.h"

#pragma comment(lib, "Ws2_32.lib")



//////////////////////////////////////////////////////////////////////////
// private parameters
//

SOCKET ServerConnection::serverSocket = NULL;

ClientList *ServerConnection::clientList = NULL;

ClientList *ServerConnection::waitingClients = NULL;

HANDLE ServerConnection::observeClientsThreadHandle = NULL;

int ServerConnection::newClientId = 0;




//////////////////////////////////////////////////////////////////////////
// public methods
//
int ServerConnection::create()
{
	if (serverSocket != NULL) {
		return SERVERSOCKET_ALREADY_CREATED;
	}

	WSADATA wsd;

    if (WSAStartup(MAKEWORD(2,2), &wsd) != 0) {
		return SERVERSOCKET_WSA_FAILED;
    }

	struct sockaddr_in local;
	int rc;

	local.sin_family = AF_INET;
	local.sin_port = htons(4444);
	local.sin_addr.s_addr = htons(INADDR_ANY);

	serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (serverSocket == INVALID_SOCKET) {
		WSACleanup();
		return SERVERSOCKET_CREATE_FAILED;
	}

	rc = bind(serverSocket, (struct sockaddr*)&local, sizeof(local));
	if (rc == SOCKET_ERROR) {
		WSACleanup();
		return SERVERSOCKET_BIND_FAILED;
	}

	rc = listen(serverSocket, 5);
	if (rc == SOCKET_ERROR) {
		WSACleanup();
		return SERVERSOCKET_LISTEN_FAILED;
	}
	
	clientList = new ClientList(CLIENTS_MAXCNT);
	waitingClients = new ClientList(CLIENTS_MAXCNT);
	
	observeClientsThreadHandle = CreateThread(NULL, 0, observeClientsThread, NULL, 0, NULL);
	
	return SERVERSOCKET_OK;
	
}

int ServerConnection::close()
{
	if (serverSocket == NULL) {
		return SERVERSOCKET_ALREADY_CLOSED;
	}

	if (observeClientsThreadHandle != NULL) {
		CloseHandle(observeClientsThreadHandle);
	}
	observeClientsThreadHandle = NULL;

	closesocket(serverSocket);
	serverSocket = NULL;

	waitingClients->removeAll();
	while ( ! clientList->isEmpty() ) {
	}

	delete clientList;
	delete waitingClients;
	clientList = NULL;
	waitingClients = NULL;

	WSACleanup();
	return SERVERSOCKET_CLOSED;
}





//////////////////////////////////////////////////////////////////////////
// private methods
//

void ServerConnection::sendRefreshDataToClients()
{
	if (clientList == NULL) {
		return;
	}
	if ( ! clientList->isEmpty() ) {
		char** names = clientList->getNames();
		RefreshData *refreshData = new RefreshData(names, clientList->getSize());
		clientList->sendDataToClients(refreshData);
		delete refreshData;
	}
}


DWORD WINAPI ServerConnection::observeClientsThread(PVOID pParam)
{
	while (TRUE) {
		SOCKET connectedSocket = accept(serverSocket, NULL, NULL);
		if (connectedSocket == INVALID_SOCKET) {
			for (int i = 0; i < clientList->getSize(); i++) {
				clientList->get(i)->close();
			}
			return INVALID_SOCKET;
		}

		CreateThread(NULL, 0, receiveRequestConnectThread, &connectedSocket, 0, NULL);
	}
	return 0;
}

DWORD WINAPI ServerConnection::receiveRequestConnectThread(PVOID pParam)
{
	SOCKET *pConnectedSocket = (SOCKET *)pParam;
	SOCKET connectedSocket = *pConnectedSocket;

	char buf[sizeof(int)];
	int received;

	received = recv(connectedSocket, buf, sizeof(buf), 0);
	if (received <= 0) {
		closesocket(connectedSocket);
		return SOCKET_ERROR;
	}
	if (received != sizeof(buf)) {
		closesocket(connectedSocket);
		return -3;
	}
	int* ptype = (int*)buf;
	int type = *ptype;
	if (type == DATATYPE_REQUEST_CLIENT_CONNECT) {
		RequestClientConnectData *d = RequestClientConnectData::recieveData(connectedSocket);
		newClientId++;

		Client *newClient = new Client(connectedSocket, newClientId, d->getName());
		delete d;

		// if server is full
		if (clientList->isFull()) {
			ClientRefusedData *crData = new ClientRefusedData();
			newClient->sendData(crData);
			delete crData;

			delete newClient;
			return 1;
		}
		
		// if client list is not empty
		if ( !clientList->isEmpty()) {
			if (waitingClients->isEmpty()) {
				RequestImageData *requestImageData = new RequestImageData();
				clientList->sendDataToClient(requestImageData, 0);
				delete requestImageData;
			}
			waitingClients->add(newClient);
		} else {
			clientList->add(newClient);
			CreateThread(NULL, 0, receiveMessagesFromClientThread, newClient, 0, NULL);

			InitData *initData = new InitData();
			newClient->sendData(initData);
			delete initData;
		}

	} else {
		closesocket(connectedSocket);
	}
	return 0;
}

DWORD WINAPI ServerConnection::receiveMessagesFromClientThread(PVOID pParam)
{
	Client *client = (Client *)pParam;

	char buf[sizeof(int)];
	int received;
	while (TRUE) {
		received = client->recvFromClient(buf, sizeof(buf));
		if (received <= 0) {
			clientList->deleteClient(client);
			sendRefreshDataToClients();
			return SOCKET_ERROR;
		}
		if (received != sizeof(buf)) {
			continue;
		}
		int* ptype = (int*)buf;
		int type = *ptype;
		switch (type) {
			case DATATYPE_IMAGE_DATA:
				{
					ImageData *imageData = ImageData::recieveData(client->getSocket());

					// if imageData was requested by server for waiting clients
					if (imageData->isRequested()) {
						int i = 0;
						while (i < waitingClients->getSize()) {
							// if server is full
							if (clientList->isFull()) {
								ClientRefusedData *crData = new ClientRefusedData();
								waitingClients->get(i)->sendData(crData);
								delete crData;

								waitingClients->deleteClient(i);
								continue;
							}

							waitingClients->get(i)->sendData(imageData);

							clientList->add(waitingClients->get(i));
							CreateThread(NULL, 0, receiveMessagesFromClientThread, waitingClients->get(i), 0, NULL);

							InitData *initData = new InitData();
							waitingClients->get(i)->sendData(initData);
							delete initData;

							i++;
						}
						sendRefreshDataToClients();
						waitingClients->removeAll();
					} else {
						if (client->getId() == clientList->get(0)->getId()) {
							clientList->sendDataToClients(imageData);
						} else {
							UnableLoadData *unableLoadData = new UnableLoadData();
							client->sendData(unableLoadData);
							delete unableLoadData;
						}
					}

					delete imageData;
					break;
				}
			case DATATYPE_IMAGE_DIMENSION_DATA:
				{
					ImageDimensionData *imageDimensionData = ImageDimensionData::recieveData(client->getSocket());
					if (client->getId() == clientList->get(0)->getId()) {
						clientList->sendDataToClients(imageDimensionData);
					} else {
						UnableResizeData *unableResizeData = new UnableResizeData();
						client->sendData(unableResizeData);
						delete unableResizeData;
					}
					delete imageDimensionData;
					break;
				}
			case DATATYPE_REQUEST_REFRESH:
				{
					sendRefreshDataToClients();
					break;
				}
			case DATATYPE_CLEAR:
				{
					if (client->getId() == clientList->get(0)->getId()) {
						ClearData *clearData = ClearData::recieveData(client->getSocket());
						clientList->sendDataToClients(clearData);
						delete clearData;
					} else {
						UnableClearData *unableClearData = new UnableClearData();
						client->sendData(unableClearData);
						delete unableClearData;
					}
					break;
				}
			case DATATYPE_PEN:
				{
					PenData *penData = PenData::recieveData(client->getSocket());
					clientList->sendDataToClientsExcept(penData, client);
					delete penData;
					break;
				}
			case DATATYPE_RECT:
				{
					RectData *rectData = RectData::recieveData(client->getSocket());
					clientList->sendDataToClientsExcept(rectData, client);
					delete rectData;
					break;
				}
			case DATATYPE_CIRCLE:
				{
					CircleData *circleData = CircleData::recieveData(client->getSocket());
					clientList->sendDataToClientsExcept(circleData, client);
					delete circleData;
					break;
				}
			case DATATYPE_ERASER:
				{
					EraserData *eraserData = EraserData::recieveData(client->getSocket());
					clientList->sendDataToClientsExcept(eraserData, client);
					delete eraserData;
					break;
				}
			case DATATYPE_REQUEST_CLIENT_CLOSE:
				{
					AcceptClientCloseData *acceptClientCloseData = new AcceptClientCloseData();
					client->sendData(acceptClientCloseData);
					delete acceptClientCloseData;

					client->close();
					clientList->deleteClient(client);
					sendRefreshDataToClients();
					return 1;
				}
		}
	}
	
	return 0;
}



