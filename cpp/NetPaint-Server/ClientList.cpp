#include "StdAfx.h"
#include "ClientList.h"



ClientList::ClientList(int maxSize)
{
	clients = (Client **)calloc(maxSize, sizeof(Client *));
	ClientList::maxSize = maxSize;
	size = 0;
}

ClientList::~ClientList()
{
	deleteAll();
	delete [] clients;
}

int ClientList::getSize()
{
	return size;
}

bool ClientList::isFull()
{
	return size >= maxSize;
}

bool ClientList::isEmpty()
{
	return size == 0;
}

char **ClientList::getNames()
{
	char** names = (char **)calloc(size, sizeof(char *));
	for (int i = 0; i < size; i++) {
		names[i] = (char *)calloc(strlen(clients[i]->getName())+1, sizeof(char *));
		strcpy_s(names[i], strlen(clients[i]->getName())+1, clients[i]->getName());
	}
	return names;
}

int ClientList::sendDataToClients(SendingData *data)
{
	if (data == NULL) {
		return -1;
	}
	for (int i = 0; i < size; i++) {
		clients[i]->sendData(data);
	}
	return 0;
}

int ClientList::sendDataToClientsExcept(SendingData *data, Client *client)
{
	if ( (data == NULL) || (client == NULL) ) {
		return -1;
	}
	for (int i = 0; i < size; i++) {
		if (clients[i]->getId() != client->getId()) {
			clients[i]->sendData(data);
		}
	}
	return 0;
}

int ClientList::sendDataToClient(SendingData *data, int index)
{
	if (data == NULL) {
		return -1;
	}
	clients[index]->sendData(data);
	return 0;
}

void ClientList::add(Client *client)
{
	if (size < maxSize) {
		clients[size] = client;
		size++;
	}
}

Client* ClientList::get(int index)
{
	if (index >= size) {
		return NULL;
	}
	return clients[index];
}

void ClientList::deleteClient(Client *client)
{
	if (client == NULL) {
		return;
	}
	int i = 0;
	while (i < size) {
		if (clients[i]->getId() == client->getId()) {
			break;
		}
		i++;
	}
	deleteClient(i);
}

void ClientList::deleteClient(int index)
{
	if ( (index < 0) || (index >= size) ) {
		return;
	}
	delete clients[index];
	while (index < size-1) {
		clients[index] = clients[index+1];
		index++;
	}
	size--;
}

void ClientList::deleteAll()
{
	for (int i = 0; i < size; i++) {
		delete clients[i];
	}
	size = 0;
}

void ClientList::removeClient(Client *client)
{
	if (client == NULL) {
		return;
	}
	int i = 0;
	while (i < size) {
		if (clients[i]->getId() == client->getId()) {
			break;
		}
		i++;
	}
	removeClient(i);
}

void ClientList::removeClient(int index)
{
	if ( (index < 0) || (index >= size) ) {
		return;
	}
	while (index < size-1) {
		clients[index] = clients[index+1];
		index++;
	}
	size--;
}

void ClientList::removeAll()
{
	size = 0;
}



