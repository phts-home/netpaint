#include "StdAfx.h"
#include <stdlib.h>

#include "SendingData.h"

#pragma comment(lib, "Ws2_32.lib")


/*
 * SendingData
 */
SendingData::SendingData()
{
}

int SendingData::getType()
{
	return type;
}


/*
 * RequestClientConnectData
 */
RequestClientConnectData::RequestClientConnectData(const char* name)
{
	type = DATATYPE_REQUEST_CLIENT_CONNECT;
	this->name = (char *)calloc(strlen(name)+1, sizeof(char));
	strcpy_s(this->name, strlen(name)+1, name);
}

RequestClientConnectData::~RequestClientConnectData()
{
	delete [] name;
}

char* RequestClientConnectData::getName()
{
	return RequestClientConnectData::name;
}

int RequestClientConnectData::sendData(SOCKET s)
{
	int namelen = (int)strlen(name);
	send(s, (char *)&type, sizeof(int), 0);

	send(s, (char *)&namelen, sizeof(int), 0);
	send(s, name, namelen, 0);
	return 0;
}

RequestClientConnectData* RequestClientConnectData::recieveData(SOCKET s)
{
	int _namelen;
	if (recv(s, (char *)&_namelen, sizeof(int), 0) <= 0) {
		return NULL;
	}

	char *_name = (char *)calloc(_namelen+1, sizeof(char));
	if (recv(s, _name, _namelen, 0) <= 0) {
		delete [] _name;
		return NULL;
	}
	_name[_namelen] = 0;
	return new RequestClientConnectData(_name);
}


/*
 * ClientRefusedData
 */
ClientRefusedData::ClientRefusedData()
{
	type = DATATYPE_CLIENT_REFUSED;
}

int ClientRefusedData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	return 0;
}


/*
 * RequestImageData
 */
RequestImageData::RequestImageData()
{
	type = DATATYPE_REQUEST_IMAGE_DATA;
}

int RequestImageData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	return 0;
}



/*
 * ImageData
 */
ImageData::ImageData(bool requested, int imageWidth, int imageHeight, const char *imageBytes, int imageBytesLength)
{
	type = DATATYPE_IMAGE_DATA;
	this->requested = requested;
	this->imageWidth = imageWidth;
	this->imageHeight = imageHeight;
	this->imageBytesLength = imageBytesLength;
	this->imageBytes = NULL;
	if (imageBytesLength > 0) {
		if (imageBytes != NULL) {
			this->imageBytes = (char *)calloc(imageBytesLength, sizeof(char));
			memcpy(this->imageBytes, imageBytes, imageBytesLength);
		}
	}
}

ImageData::~ImageData()
{
	delete [] imageBytes;
}

bool ImageData::isRequested()
{
	return requested;
}

int ImageData::getImageWidth()
{
	return imageWidth;
}

int ImageData::getImageHeight()
{
	return imageHeight;
}

int ImageData::getImageBytesLength()
{
	return imageBytesLength;
}

char* ImageData::getImageBytes()
{
	return imageBytes;
}

int ImageData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	send(s, (char *)&requested, sizeof(bool), 0);
	send(s, (char *)&imageWidth, sizeof(int), 0);
	send(s, (char *)&imageHeight, sizeof(int), 0);
	send(s, (char *)&imageBytesLength, sizeof(int), 0);

	if (imageBytesLength == 0) {
		return 0;
	}

	char buf[1024];
	int sent = 0;
	int size = 0;
	int n = 0;
	while (sent < imageBytesLength) {
		for (size = 0; size < sizeof(buf); size++) {
			buf[size] = imageBytes[n];
			n++;
			if (n >= imageBytesLength) {
				size++;
				break;
			}
		}
		sent += send(s, buf, size, 0);
	}
	return 0;
}

ImageData* ImageData::recieveData(SOCKET s)
{
	bool _requested;
	recv(s, (char *)&_requested, sizeof(bool), 0);
	int _imageWidth;
	recv(s, (char *)&_imageWidth, sizeof(int), 0);
	int _imageHeight;
	recv(s, (char *)&_imageHeight, sizeof(int), 0);
	int _imageBytesLength;
	recv(s, (char *)&_imageBytesLength, sizeof(int), 0);
	char *_imageBytes = NULL;
	if (_imageBytesLength > 0) {
		_imageBytes = (char *)calloc(_imageBytesLength, sizeof(char));
		int received = 0;
		int newReceived = 0;
		char *_buf = (char *)calloc(_imageBytesLength, sizeof(char));
		while (received != _imageBytesLength) {
			newReceived = recv(s, _buf, _imageBytesLength-received, 0);
			for (int i = received; i < received+newReceived; i++) {
				_imageBytes[i] = _buf[i-received];
			}
			received += newReceived;
		}
		delete [] _buf;
	}
	return new ImageData(_requested, _imageWidth, _imageHeight, _imageBytes, _imageBytesLength);
}



/*
 * ImageDimensionData
 */
ImageDimensionData::ImageDimensionData(int imageWidth, int imageHeight)
{
	type = DATATYPE_IMAGE_DIMENSION_DATA;
	this->imageWidth = imageWidth;
	this->imageHeight = imageHeight;
}

int ImageDimensionData::getImageWidth()
{
	return imageWidth;
}

int ImageDimensionData::getImageHeight()
{
	return imageHeight;
}

int ImageDimensionData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	send(s, (char *)&imageWidth, sizeof(int), 0);
	send(s, (char *)&imageHeight, sizeof(int), 0);
	return 0;
}

ImageDimensionData* ImageDimensionData::recieveData(SOCKET s)
{
	int _imageWidth;
	recv(s, (char *)&_imageWidth, sizeof(int), 0);
	int _imageHeight;
	recv(s, (char *)&_imageHeight, sizeof(int), 0);
	return new ImageDimensionData(_imageWidth, _imageHeight);
}




/*
 * InitData
 */
InitData::InitData()
{
	type = DATATYPE_INIT;
}

int InitData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	return 0;
}




/*
 * RequestRefreshData
 */
RequestRefreshData::RequestRefreshData()
{
	type = DATATYPE_REQUEST_REFRESH;
}

int RequestRefreshData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	return 0;
}




/*
 * RefreshData
 */
RefreshData::RefreshData(char** names, int clientCount)
{
	type = DATATYPE_REFRESH;
	this->names = names;
	this->clientCount = clientCount;
}

RefreshData::~RefreshData()
{
	for (int i = 0; i < clientCount; i++) {
		delete [] names[i];
	}
	delete [] names;
}

char** RefreshData::getNames()
{
	return names;
}

int RefreshData::getClientCount()
{
	return clientCount;
}

int RefreshData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	send(s, (char *)&clientCount, sizeof(int), 0);
	int len;
	for (int i = 0; i < clientCount; i++) {
		len = (int)strlen(names[i]);
		send(s, (char *)&len, sizeof(int), 0);
		send(s, names[i], len, 0);
	}
	return 0;
}

RefreshData* RefreshData::recieveData(SOCKET s)
{
	int _clientCount;
	recv(s, (char *)&_clientCount, sizeof(int), 0);
	char** _names = (char **)calloc(_clientCount, sizeof(char *));
	int _len;
	for (int i = 0; i < _clientCount; i++) {
		recv(s, (char *)&_len, sizeof(int), 0);
		_names[i] = (char *)calloc(_len+1, sizeof(char));
		recv(s, _names[i], _len, 0);
		_names[i][_len] = 0;
	}
	return new RefreshData(_names, _clientCount);
}


/*
 * ClearData
 */
ClearData::ClearData()
{
	type = DATATYPE_CLEAR;
}

int ClearData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	return 0;
}

ClearData* ClearData::recieveData(SOCKET s)
{
	return new ClearData();
}




/*
 * PenData
 */
PenData::PenData(int *coords, int coordsLength, int red, int green, int blue, int penWidth)
{
	type = DATATYPE_PEN;
	this->coords = coords;
	this->coordsLength = coordsLength;
	this->red = red;
	this->green = green;
	this->blue = blue;
	this->penWidth = penWidth;
}
PenData::~PenData()
{
	delete [] coords;
}

int* PenData::getCoords()
{
	return coords;
}

int PenData::getCoordsLength()
{
	return coordsLength;
}

int PenData::getRed()
{
	return red;
}

int PenData::getGreen()
{
	return green;
}

int PenData::getBlue()
{
	return blue;
}

int PenData::getPenWidth()
{
	return penWidth;
}

int PenData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	send(s, (char *)&coordsLength, sizeof(int), 0);
	send(s, (char *)coords, coordsLength*sizeof(int), 0);
	send(s, (char *)&red, sizeof(int), 0);
	send(s, (char *)&green, sizeof(int), 0);
	send(s, (char *)&blue, sizeof(int), 0);
	send(s, (char *)&penWidth, sizeof(int), 0);
	return 0;
}

PenData* PenData::recieveData(SOCKET s)
{
	int *_coords, _coordsLength, _red, _green, _blue, _penWidth;
	recv(s, (char *)&_coordsLength, sizeof(int), 0);

	_coords = (int *)calloc(_coordsLength, sizeof(int));
	int received = recv(s, (char *)_coords, _coordsLength*sizeof(int), 0);

	recv(s, (char *)&_red, sizeof(int), 0);
	recv(s, (char *)&_green, sizeof(int), 0);
	recv(s, (char *)&_blue, sizeof(int), 0);
	recv(s, (char *)&_penWidth, sizeof(int), 0);
	return new PenData(_coords, _coordsLength, _red, _green, _blue, _penWidth);
}




/*
 * RectData
 */
RectData::RectData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth)
{
	type = DATATYPE_RECT;
	this->x1 = x1;
	this->y1 = y1;
	this->x2 = x2;
	this->y2 = y2;
	this->red = red;
	this->green = green;
	this->blue = blue;
	this->penWidth = penWidth;
}
RectData::~RectData()
{
}

int RectData::getX1()
{
	return x1;
}

int RectData::getY1()
{
	return y1;
}

int RectData::getX2()
{
	return x2;
}

int RectData::getY2()
{
	return y2;
}

int RectData::getRed()
{
	return red;
}

int RectData::getGreen()
{
	return green;
}

int RectData::getBlue()
{
	return blue;
}

int RectData::getPenWidth()
{
	return penWidth;
}

int RectData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	send(s, (char *)&x1, sizeof(int), 0);
	send(s, (char *)&y1, sizeof(int), 0);
	send(s, (char *)&x2, sizeof(int), 0);
	send(s, (char *)&y2, sizeof(int), 0);
	send(s, (char *)&red, sizeof(int), 0);
	send(s, (char *)&green, sizeof(int), 0);
	send(s, (char *)&blue, sizeof(int), 0);
	send(s, (char *)&penWidth, sizeof(int), 0);
	return 0;
}

RectData* RectData::recieveData(SOCKET s)
{
	int _x1, _y1, _x2, _y2, _red, _green, _blue, _penWidth;
	recv(s, (char *)&_x1, sizeof(int), 0);
	recv(s, (char *)&_y1, sizeof(int), 0);
	recv(s, (char *)&_x2, sizeof(int), 0);
	recv(s, (char *)&_y2, sizeof(int), 0);
	recv(s, (char *)&_red, sizeof(int), 0);
	recv(s, (char *)&_green, sizeof(int), 0);
	recv(s, (char *)&_blue, sizeof(int), 0);
	recv(s, (char *)&_penWidth, sizeof(int), 0);
	return new RectData(_x1, _y1, _x2, _y2, _red, _green, _blue, _penWidth);
}



/*
 * CircleData
 */
CircleData::CircleData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth)
{
	type = DATATYPE_CIRCLE;
	this->x1 = x1;
	this->y1 = y1;
	this->x2 = x2;
	this->y2 = y2;
	this->red = red;
	this->green = green;
	this->blue = blue;
	this->penWidth = penWidth;
}
CircleData::~CircleData()
{
}

int CircleData::getX1()
{
	return x1;
}

int CircleData::getY1()
{
	return y1;
}

int CircleData::getX2()
{
	return x2;
}

int CircleData::getY2()
{
	return y2;
}

int CircleData::getRed()
{
	return red;
}

int CircleData::getGreen()
{
	return green;
}

int CircleData::getBlue()
{
	return blue;
}

int CircleData::getPenWidth()
{
	return penWidth;
}

int CircleData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	send(s, (char *)&x1, sizeof(int), 0);
	send(s, (char *)&y1, sizeof(int), 0);
	send(s, (char *)&x2, sizeof(int), 0);
	send(s, (char *)&y2, sizeof(int), 0);
	send(s, (char *)&red, sizeof(int), 0);
	send(s, (char *)&green, sizeof(int), 0);
	send(s, (char *)&blue, sizeof(int), 0);
	send(s, (char *)&penWidth, sizeof(int), 0);
	return 0;
}

CircleData* CircleData::recieveData(SOCKET s)
{
	int _x1, _y1, _x2, _y2, _red, _green, _blue, _penWidth;
	recv(s, (char *)&_x1, sizeof(int), 0);
	recv(s, (char *)&_y1, sizeof(int), 0);
	recv(s, (char *)&_x2, sizeof(int), 0);
	recv(s, (char *)&_y2, sizeof(int), 0);
	recv(s, (char *)&_red, sizeof(int), 0);
	recv(s, (char *)&_green, sizeof(int), 0);
	recv(s, (char *)&_blue, sizeof(int), 0);
	recv(s, (char *)&_penWidth, sizeof(int), 0);
	return new CircleData(_x1, _y1, _x2, _y2, _red, _green, _blue, _penWidth);
}


/*
 * EraserData
 */
EraserData::EraserData(int *coords, int coordsLength, int penWidth)
{
	type = DATATYPE_ERASER;
	this->coords = coords;
	this->coordsLength = coordsLength;
	this->penWidth = penWidth;
}
EraserData::~EraserData()
{
	delete [] coords;
}

int* EraserData::getCoords()
{
	return coords;
}

int EraserData::getCoordsLength()
{
	return coordsLength;
}

int EraserData::getPenWidth()
{
	return penWidth;
}

int EraserData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	send(s, (char *)&coordsLength, sizeof(int), 0);
	send(s, (char *)coords, coordsLength*sizeof(int), 0);
	send(s, (char *)&penWidth, sizeof(int), 0);
	return 0;
}

EraserData* EraserData::recieveData(SOCKET s)
{
	int *_coords, _coordsLength, _penWidth;
	recv(s, (char *)&_coordsLength, sizeof(int), 0);

	_coords = (int *)calloc(_coordsLength, sizeof(int));
	int received = recv(s, (char *)_coords, _coordsLength*sizeof(int), 0);

	recv(s, (char *)&_penWidth, sizeof(int), 0);
	return new EraserData(_coords, _coordsLength, _penWidth);
}






/*
 * RequestClientCloseData
 */
RequestClientCloseData::RequestClientCloseData()
{
	type = DATATYPE_REQUEST_CLIENT_CLOSE;
}

int RequestClientCloseData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	return 0;
}




/*
 * AcceptClientCloseData
 */
AcceptClientCloseData::AcceptClientCloseData()
{
	type = DATATYPE_ACCEPT_CLIENT_CLOSE;
}

int AcceptClientCloseData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	return 0;
}



/*
 * UnableResizeData
 */
UnableResizeData::UnableResizeData()
{
	type = DATATYPE_UNABLE_RESIZE;
}

int UnableResizeData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	return 0;
}



/*
 * UnableClearData
 */
UnableClearData::UnableClearData()
{
	type = DATATYPE_UNABLE_CLEAR;
}

int UnableClearData::sendData(SOCKET s)
{
	send(s, (char *)&type, sizeof(int), 0);
	return 0;
}



/*
 * UnableLoadData
 */
UnableLoadData::UnableLoadData()
{
	type = DATATYPE_UNABLE_LOAD;
}

int UnableLoadData::sendData( SOCKET s )
{
	send(s, (char *)&type, sizeof(int), 0);
	return 0;
}