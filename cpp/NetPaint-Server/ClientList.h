#pragma once

#include "SendingData.h"
#include "Client.h"


class ClientList
{
public:
	ClientList(int maxSize);
	~ClientList();

public:
	int getSize();
	bool isFull();
	bool isEmpty();
	char **getNames();
	int sendDataToClients(SendingData *data);
	int sendDataToClientsExcept(SendingData *data, Client *client);
	int sendDataToClient(SendingData *data, int index);
	void add(Client *client);
	Client* get(int index);
	void deleteClient(Client *client);
	void deleteClient(int index);
	void deleteAll();
	void removeClient(Client *client);
	void removeClient(int index);
	void removeAll();
	

private:
	Client **clients;
	int size;
	int maxSize;
};
