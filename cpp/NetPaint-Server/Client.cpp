#include "StdAfx.h"
#include "Client.h"


#include "SendingData.h"


Client::Client(SOCKET socket, int id, char *_name)
{
	this->socket = socket;
	this->id = id;
	this->name = (char *)calloc(strlen(_name)+1, sizeof(char));
	strcpy_s(this->name, strlen(_name)+1, _name);
}

Client::~Client()
{
	close();
	delete [] name;
}





int Client::close()
{
	return closesocket(socket);
}

SOCKET Client::getSocket()
{
	return socket;
}

int Client::getId() const
{
	return id;
}

char* Client::getName() const
{
	return name;
}

void Client::setId(int id)
{
	this->id = id;
}

int Client::recvFromClient(char *buf, int len)
{
	return recv(socket, buf, len, 0);
}

int Client::sendData(SendingData *data)
{
	return data->sendData(socket);
}