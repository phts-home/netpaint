#pragma once


#include "Winsock2.h"


#define DATATYPE_REQUEST_CLIENT_CONNECT 1
#define DATATYPE_CLIENT_REFUSED 2
#define DATATYPE_REQUEST_IMAGE_DATA 3
#define DATATYPE_IMAGE_DATA 4
#define DATATYPE_IMAGE_DIMENSION_DATA 5
#define DATATYPE_INIT 6
#define DATATYPE_REQUEST_REFRESH 7
#define DATATYPE_REFRESH 8
#define DATATYPE_CLEAR 11
#define DATATYPE_PEN 12
#define DATATYPE_RECT 13
#define DATATYPE_CIRCLE 14
#define DATATYPE_ERASER 15
#define DATATYPE_REQUEST_CLIENT_CLOSE 21
#define DATATYPE_ACCEPT_CLIENT_CLOSE 22
#define DATATYPE_UNABLE_RESIZE 31
#define DATATYPE_UNABLE_CLEAR 32
#define DATATYPE_UNABLE_LOAD 33




/*
 * SendingData
 */
class SendingData abstract
{
public:
	SendingData();

public:
	int getType();
	virtual int sendData(SOCKET s) = 0;

protected:
	int type;
};


/*
 * RequestClientConnectData
 */
class RequestClientConnectData : public SendingData
{
public:
	RequestClientConnectData(const char *name);
	~RequestClientConnectData();

public:
	char *getName();
	int sendData(SOCKET s);
	static RequestClientConnectData *recieveData(SOCKET s);

private:
	char *name;
};


/*
 * ClientRefusedData
 */
class ClientRefusedData : public SendingData
{
public:
	ClientRefusedData();

public:
	int sendData(SOCKET s);
};


/*
 * RequestImageData
 */
class RequestImageData : public SendingData
{
public:
	RequestImageData();

public:
	int sendData(SOCKET s);
};


/*
 * ImageData
 */
class ImageData : public SendingData
{
public:
	ImageData(bool requested, int imageWidth, int imageHeight, const char *imageBytes, int imageBytesLength);
	~ImageData();

public:
	bool isRequested();
	int getImageWidth();
	int getImageHeight();
	int getImageBytesLength();
	char *getImageBytes();
	int sendData(SOCKET s);
	static ImageData *recieveData(SOCKET s);

private:
	bool requested;
	int imageWidth;
	int imageHeight;
	int imageBytesLength;
	char *imageBytes;
};



/*
 * ImageDimensionData
 */
class ImageDimensionData : public SendingData
{
public:
	ImageDimensionData(int imageWidth, int imageHeight);

public:
	int getImageWidth();
	int getImageHeight();
	int sendData(SOCKET s);
	static ImageDimensionData *recieveData(SOCKET s);

private:
	int imageWidth;
	int imageHeight;
};



/*
 * InitData
 */
class InitData : public SendingData
{
public:
	InitData();

public:
	int sendData(SOCKET s);
};


/*
 * RequestRefreshData
 */
class RequestRefreshData : public SendingData
{
public:
	RequestRefreshData();

public:
	int sendData(SOCKET s);
};


/*
 * RefreshData
 */
class RefreshData : public SendingData
{
public:
	RefreshData(char **names, int clientCount);
	~RefreshData();

public:
	char **getNames();
	int getClientCount();
	int sendData(SOCKET s);
	static RefreshData *recieveData(SOCKET s);

private:
	char **names;
	int clientCount;
};





/*
 * ClearData
 */
class ClearData : public SendingData
{
public:
	ClearData();

public:
	int sendData(SOCKET s);
	static ClearData *recieveData(SOCKET s);
};

/*
 * PenData
 */
class PenData : public SendingData
{
public:
	PenData(int *coords, int coordsLength, int red, int green, int blue, int penWidth);
	~PenData();

public:
	int *getCoords();
	int getCoordsLength();
	int getRed();
	int getGreen();
	int getBlue();
	int getPenWidth();
	int sendData(SOCKET s);
	static PenData *recieveData(SOCKET s);

private:
	int *coords;
	int coordsLength;
	int red;
	int green;
	int blue;
	int penWidth;
};


/*
 * RectData
 */
class RectData : public SendingData
{
public:
	RectData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth);
	~RectData();

public:
	int getX1();
	int getY1();
	int getX2();
	int getY2();
	int getRed();
	int getGreen();
	int getBlue();
	int getPenWidth();
	int sendData(SOCKET s);
	static RectData *recieveData(SOCKET s);

private:
	int x1;
	int y1;
	int x2;
	int y2;
	int red;
	int green;
	int blue;
	int penWidth;
};


/*
 * CircleData
 */
class CircleData : public SendingData
{
public:
	CircleData(int x1, int y1, int x2, int y2, int red, int green, int blue, int penWidth);
	~CircleData();

public:
	int getX1();
	int getY1();
	int getX2();
	int getY2();
	int getRed();
	int getGreen();
	int getBlue();
	int getPenWidth();
	int sendData(SOCKET s);
	static CircleData *recieveData(SOCKET s);

private:
	int x1;
	int y1;
	int x2;
	int y2;
	int red;
	int green;
	int blue;
	int penWidth;
};


/*
 * EraserData
 */
class EraserData : public SendingData
{
public:
	EraserData(int *coords, int coordsLength, int penWidth);
	~EraserData();

public:
	int *getCoords();
	int getCoordsLength();
	int getPenWidth();
	int sendData(SOCKET s);
	static EraserData *recieveData(SOCKET s);

private:
	int *coords;
	int coordsLength;
	int penWidth;
};



/*
 * RequestClientCloseData
 */
class RequestClientCloseData : public SendingData
{
public:
	RequestClientCloseData();

public:
	int sendData(SOCKET s);
};


/*
 * AcceptClientCloseData
 */
class AcceptClientCloseData : public SendingData
{
public:
	AcceptClientCloseData();

public:
	int sendData(SOCKET s);
};



/*
 * UnableResizeData
 */
class UnableResizeData : public SendingData
{
public:
	UnableResizeData();

public:
	int sendData(SOCKET s);
};



/*
 * UnableClearData
 */
class UnableClearData : public SendingData
{
public:
	UnableClearData();

public:
	int sendData(SOCKET s);
};



/*
 * UnableLoadData
 */
class UnableLoadData : public SendingData
{
public:
	UnableLoadData();

public:
	int sendData(SOCKET s);
};


